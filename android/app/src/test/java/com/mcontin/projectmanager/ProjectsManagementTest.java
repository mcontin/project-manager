package com.mcontin.projectmanager;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mcontin.projectmanager.models.ErrorResponse;
import com.mcontin.projectmanager.models.Project;

import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;


/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ProjectsManagementTest {

    @Test
    public void shouldRetrieveProjectsAndMapThem() {
//        Example responses:
//        - no project found (single object):
//            {
//                "name": "NotFound",
//                "detail": "No project was found for user mc"
//            }
//        - snippets of projects found (array of objects):
//            [
//                {
//                    "code": "codice",
//                    "name": "progetto432"
//                },
//                {
//                    "code": "prova postman",
//                    "name": "progetto432"
//                }
//            ]

        ErrorResponse mockError = new ErrorResponse("NotFound", "No project was found for user mc");
        ObjectMapper mapper = new ObjectMapper();

        Project.Snippet snippetA = new Project.Snippet("code1", "name1");
        Project.Snippet snippetB = new Project.Snippet("code2", "name2");
        Project.Snippet snippetC = new Project.Snippet("code3", "name3");

        List<Project.Snippet> projects = new ArrayList<>();
        projects.add(snippetA);
        projects.add(snippetB);
        projects.add(snippetC);

        byte[] bytesProjects = null;
        byte[] bytesError = null;
        try {
            bytesError = mapper.writeValueAsBytes(mockError);
            bytesProjects = mapper.writeValueAsBytes(projects);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        ErrorResponse retrievedError = null;
        try {
            projects = mapper.readValue(bytesProjects,
                new TypeReference<List<Project.Snippet>>() {});
            retrievedError = mapper.readValue(bytesError, ErrorResponse.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        assertEquals(snippetA, projects.get(0));
        assertEquals(snippetB, projects.get(1));
        assertEquals(snippetC, projects.get(2));
        assertEquals(retrievedError, mockError);

    }

}