package com.mcontin.projectmanager.views;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by mattia on 22/06/16.
 */
public class SmartProgressDialog{

    private static ProgressDialog mDialog;

    public static void show(Context aContext, String aMessage) {
        if(aContext != null)
            if(mDialog == null) {
                mDialog = new ProgressDialog(aContext);
                mDialog.setTitle("Loading");
                mDialog.setCancelable(false);
                mDialog.setMessage(aMessage);
            }
            mDialog.show();
    }


    public static void dismiss() {
        if(mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
        }
    }

}
