package com.mcontin.projectmanager.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mcontin.projectmanager.models.GetDetailsListener;
import com.mcontin.projectmanager.R;
import com.mcontin.projectmanager.models.Snippet;

import java.util.List;

/**
 * Created by mattia on 20/06/16.
 */
public class ProjectsAdapter extends RecyclerView.Adapter<ProjectsAdapter.CardViewHolder> {

    private List<Snippet> mProjects;
    private GetDetailsListener mDetailsListener;

    public ProjectsAdapter(List<Snippet> aList, GetDetailsListener aDetailsListener) {
        mProjects = aList;
        mDetailsListener = aDetailsListener;
    }

    @Override
    public ProjectsAdapter.CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // an empty view is added to the pool of views that will be
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card, parent, false);
        return new CardViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ProjectsAdapter.CardViewHolder holder, int i) {
        final int position = i;
        // a view is taken from the pool and added to the view to be shown
        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDetailsListener.onDetailsRequested(mProjects.get(position).getCode());
            }
        });
        holder.code.setText(mProjects.get(position).getCode());
        holder.name.setText(mProjects.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return mProjects.size();
    }

    public static class CardViewHolder extends RecyclerView.ViewHolder {
        private CardView card;
        public TextView code;
        private TextView name;

        CardViewHolder(View itemView) {
            super(itemView);
            card = (CardView) itemView.findViewById(R.id.card);
            code = (TextView) itemView.findViewById(R.id.code);
            name = (TextView) itemView.findViewById(R.id.name);
        }

        public String getCode() {
            return code.getText().toString();
        }
    }
}
