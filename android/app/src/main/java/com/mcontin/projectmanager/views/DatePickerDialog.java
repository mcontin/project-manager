package com.mcontin.projectmanager.views;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.app.DatePickerDialog.OnDateSetListener;

import java.util.Calendar;

/**
 * Created by mattia on 21/06/16.
 */
public class DatePickerDialog extends DialogFragment{

//    -----------------FIELDS------------------
    private static OnDateSetListener mListener;

    public static DatePickerDialog newInstance(OnDateSetListener aListener) {
        mListener = aListener;
        return new DatePickerDialog();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new android.app.DatePickerDialog(getContext(), mListener, year, month, day);
    }

}
