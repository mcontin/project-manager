package com.mcontin.projectmanager.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.app.DatePickerDialog.OnDateSetListener;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.mcontin.projectmanager.ConnectionOpenException;
import com.mcontin.projectmanager.R;
import com.mcontin.projectmanager.models.ErrorResponse;
import com.mcontin.projectmanager.models.Project;
import com.mcontin.projectmanager.models.User;
import com.mcontin.projectmanager.util.ConnectionManager;
import com.mcontin.projectmanager.util.RestClient;
import com.mcontin.projectmanager.views.DatePickerDialog;
import com.mcontin.projectmanager.views.SmartProgressDialog;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ByteArrayEntity;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.protocol.HTTP;

public class EditProjectFragment extends Fragment
        implements Validator.ValidationListener,
        OnDateSetListener{

//  -----------------STATIC FIELDS-----------------
    private static final String TAG = "EditProjectFragment";
    private static final String TAG_PROJECT = "project";
    private static final String TAG_TOOLS = "tools";

//  -----------------VIEWS---------------------
    private EditText mEditCode;
    @NotEmpty
    private EditText mEditName;
    @NotEmpty
    private EditText mEditDescription;
    @NotEmpty
    private EditText mEditBeginDate;

    private Spinner mEditType;
    @NotEmpty
    private AutoCompleteTextView mEditManager;

    private Button mConfirmButton;
    private List<CheckBox> mCheckboxes;

    //  -----------------FIELDS------------------
    private Project mProject;
    private Project mEditedProject;
    private List<String> mTools;
    private Validator mValidator;
    private Calendar mBeginDate;
    private OnDateSetListener mInstance;
    private List<String> mUsersList;

    public EditProjectFragment() {
        // Required empty public constructor
    }

    public static EditProjectFragment newInstance(Project aProject) {
        EditProjectFragment fragment = new EditProjectFragment();
        Bundle args = new Bundle();
        args.putParcelable(TAG_PROJECT, aProject);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mInstance = this;

        mValidator = new Validator(this);
        mValidator.setValidationListener(this);

        mUsersList = new ArrayList<>();
        mCheckboxes = new ArrayList<>();
        if (getArguments() != null) {
            mProject = getArguments().getParcelable(TAG_PROJECT);
        }

        if(savedInstanceState != null) {
            mProject = savedInstanceState.getParcelable(TAG_PROJECT);
        }

        mTools = new ArrayList<>(mProject.getTools());
        for(String tool : mTools) {
            tool = tool.toLowerCase();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vRootView = inflater.inflate(R.layout.fragment_edit_project, container, false);

        setupViews(vRootView);

        return vRootView;
    }

    private void onCheckboxClicked(CheckBox aBox) {
        boolean vChecked = aBox.isChecked();
        String vText = aBox.getText().toString().toLowerCase();

        if (vChecked) {
            mTools.add(vText);
        } else {
            mTools.remove(vText);
        }

        Collections.sort(mTools);

    }

    private void setupViews(View vRootView) {
        //get all the views
        mEditCode = (EditText) vRootView.findViewById(R.id.input_edit_code);
        mEditName = (EditText) vRootView.findViewById(R.id.input_edit_name);
        mEditDescription = (EditText) vRootView.findViewById(R.id.input_edit_description);
        mEditBeginDate = (EditText) vRootView.findViewById(R.id.input_edit_begin_date);
        mEditType = (Spinner) vRootView.findViewById(R.id.spinner_edit_type);
        mEditManager = (AutoCompleteTextView) vRootView.findViewById(R.id.input_edit_manager);
        mConfirmButton = (Button) vRootView.findViewById(R.id.submit_edit);

        //write data on the views
        mEditCode.setText(mProject.getCode());
        mEditName.setText(mProject.getName());
        mEditDescription.setText(mProject.getDescription());
        //format and write the date
        SimpleDateFormat vDateFormat = new SimpleDateFormat("EEEE dd, MMMM yyyy");
        String vFormattedDate = vDateFormat.format(mProject.getBeginDate().getTime());
        mEditBeginDate.setText(vFormattedDate);
        mEditBeginDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog.newInstance(mInstance).show(getFragmentManager(), "datePicker");
            }
        });
        mBeginDate = Calendar.getInstance();
        mBeginDate.setTime(mProject.getBeginDate());
        //write the type by comparing the value from the project to those in the adapter
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.type_array, R.layout.spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mEditType.setAdapter(adapter);
        mEditType.setSelection(adapter.getPosition(mProject.getType()));
        mEditManager.setText(mProject.getManagerId());

        //get the checkboxes
        GridLayout vToolsContainer = (GridLayout) vRootView.findViewById(R.id.layout_edit_tools);
        CheckBox vCheckBox;
        for(int i = 0; i < vToolsContainer.getChildCount(); ++i) {
            if(vToolsContainer.getChildAt(i) instanceof CheckBox) {
                vCheckBox = (CheckBox) vToolsContainer.getChildAt(i);

                //if the current checkbox value is contained in the tools of the project, check it
                if(mProject.getTools().contains(vCheckBox.getText().toString().toLowerCase()))
                    vCheckBox.setChecked(true);

                vCheckBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onCheckboxClicked((CheckBox) view);
                    }
                });
                mCheckboxes.add(vCheckBox);
            }
        }

        mEditManager.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length() == 3)
                    getFilteredUsers(charSequence.toString());
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        mConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mValidator.validate();
            }
        });
    }

    private void getFilteredUsers(String userInput) {
        if (ConnectionManager.isConnected(getContext())) {
            RestClient.getFilteredUsers(userInput,
                    new JsonHttpResponseHandler() {
                private ObjectMapper mapper;

                /**
                 * Instantiate Json mapper and show progress dialog
                 */
                @Override
                public void onStart() {
                    mapper = new ObjectMapper();
                }

                /**
                 * Called when the response is successful and is a list of projects
                 *
                 * @param statusCode code of the response, i.e. 200
                 * @param response   array of projects
                 */
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    List<User> users = new ArrayList<>();
                    mUsersList.clear();

                    try {
                        users = mapper.readValue(response.toString(),
                                new TypeReference<List<User>>() {
                                });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    for (User user : users) {
                        mUsersList.add(user.getId());
                    }

                    setupUsersSuggestion();
                }

                /**
                 * If the call fails, either display the error sent by the server or
                 * a generic timeout error
                 */
                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    Log.e(TAG, "onFailure: Code: " + statusCode);
                    Log.e(TAG, "onFailure: Error: " + throwable.getMessage());

                    if (errorResponse != null) {
                        //error thrown by the server
                        Log.e(TAG, "onFailure: ResponseJson: " + errorResponse.toString());
                        try {
                            ErrorResponse error = mapper.readValue(errorResponse.toString(), ErrorResponse.class);
                            if (getContext() != null) {
                                Toast.makeText(getContext(), error.getDetail(), Toast.LENGTH_SHORT).show();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        return;
                    }

                    if (statusCode == 0 && getContext() != null)
                        //timeout
                        Toast.makeText(getContext(), "Connection error, server didn't respond", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFinish() {
                }
            });

        }
    }

    @Override
    public void onValidationSucceeded() {
        mEditedProject = new Project(
                mEditCode.getText().toString(),
                mEditName.getText().toString(),
                mEditDescription.getText().toString(),
                mProject.getCreationDate().getTime(),
                mBeginDate.getTime().getTime(),
                mEditType.getSelectedItem().toString(),
                mEditManager.getText().toString(),
                new HashSet<>(mTools),
                mProject.getCreatorId());

        if(mEditedProject.equals(mProject)) {
            Toast.makeText(getContext(), "No change detected", Toast.LENGTH_SHORT).show();
            return;
        }

        ObjectMapper mapper = new ObjectMapper();
        String jsonString = "";
        try {
            jsonString = mapper.writeValueAsString(mEditedProject);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        ByteArrayEntity vPayload = null;
        try {
            vPayload = new ByteArrayEntity(jsonString.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        assert vPayload != null;
        vPayload.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

        try {
            RestClient.update(mEditedProject,
                    getContext(),
                    new AsyncHttpResponseHandler() {

                        @Override
                        public void onStart() {
                            SmartProgressDialog.show(getContext(), "Updating project...");
                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            if(getContext() != null)
                                Toast.makeText(getContext(), "Project updated!", Toast.LENGTH_LONG).show();

                            Log.e(TAG, "onSuccess: Code: " + statusCode);
                            if(responseBody != null)
                                Log.e(TAG, "onSuccess: Response: " + new String(responseBody));

                            //We need to force the call otherwise the connection will never be closed
                            onFinish();
                            getFragmentManager().popBackStackImmediate();
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            if(getContext() != null)
                                Toast.makeText(getContext(), "Failure", Toast.LENGTH_LONG).show();

                            Log.i(TAG, "onFailure: Code: " + statusCode);
                            if(responseBody != null)
                                Log.i(TAG, "onFailure: ResponseString: " + new String(responseBody));
                            Log.e(TAG, "onFailure: Error: ", error);

                            if (statusCode == 0 && getContext() != null)
                                Toast.makeText(getContext(), "Connection error, server is probably down", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFinish() {
                            SmartProgressDialog.dismiss();
                            RestClient.closeConnection();
                        }

                    });
        } catch (ConnectionOpenException e) {
            e.printStackTrace();
            SmartProgressDialog.show(getContext(), "Updating project...");
        }


    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for(ValidationError error : errors) {
            View view = error.getView();
            List<Rule> rules = error.getFailedRules();

            final String failureMessage = rules.get(0).getMessage(getContext());
            if (view instanceof EditText) {
                EditText failed = (EditText) view;
                failed.requestFocus();
                failed.setError(failureMessage);
            } else {
                Toast.makeText(getContext(), failureMessage, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        SmartProgressDialog.dismiss();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        mBeginDate = Calendar.getInstance();
        mBeginDate.set(year, month, day);

        SimpleDateFormat vDateFormat = new SimpleDateFormat("EEEE dd, MMMM yyyy");

        String vFormattedDate = vDateFormat.format(mBeginDate.getTime());

        mEditBeginDate.setText(vFormattedDate);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelable(TAG_PROJECT, mEditedProject);
    }

    private void setupUsersSuggestion() {
        // Get the string array
        final String[] users = mUsersList.toArray(new String[0]);
        // Create the adapter and set it to the AutoCompleteTextView
        ArrayAdapter<String> managerAdapter =
                new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, users);
        mEditManager.setAdapter(managerAdapter);
    }


}
