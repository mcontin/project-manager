package com.mcontin.projectmanager.views;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.mcontin.projectmanager.models.DeletionListener;

/**
 * Created by mattia on 23/06/16.
 */
public class ConfirmDeletionDialog extends DialogFragment {

    private static DeletionListener mListener;

    public static ConfirmDeletionDialog newInstance(DeletionListener aListener) {
        mListener = aListener;
        return new ConfirmDeletionDialog();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder vBuilder = new AlertDialog.Builder(getContext());
        vBuilder.setTitle("Delete project")
                .setMessage("Are you sure you want to delete the project?")
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mListener.onConfirmDelete();
                    }
                 })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dismiss();
                    }
                });

        return vBuilder.create();
    }
}
