package com.mcontin.projectmanager.util;

import android.content.Context;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.mcontin.projectmanager.ConnectionOpenException;
import com.mcontin.projectmanager.models.Project;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.entity.ByteArrayEntity;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.protocol.HTTP;

/**
 * Created by mattia on 21/06/16.
 */
public class RestClient {

//    private static final String BASE_URL = "http://10.0.2.2:9000";
    private static final String BASE_URL = "http://192.168.43.79:9000";
//    private static final String BASE_URL = "http://172.27.1.89:9001";

    private static boolean mConnectionOpen = false;

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(Context context, String url, String contentType, HttpEntity payload, AsyncHttpResponseHandler responseHandler) {
        client.post(context, getAbsoluteUrl(url), payload, contentType, responseHandler);
    }

    public static void put(Project aProject, Context aContext, AsyncHttpResponseHandler aResponseHandler) {

        ObjectMapper mapper = new ObjectMapper();
        String jsonString = "";
        try {
            jsonString = mapper.writeValueAsString(aProject);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        ByteArrayEntity vPayload = null;
        try {
            vPayload = new ByteArrayEntity(jsonString.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        assert vPayload != null;
        vPayload.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

        client.put(aContext, getAbsoluteUrl(ServiceCalls.edit(aProject.getCode())), vPayload, "application/json", aResponseHandler);
    }

    public static void getAllProjectsOf(String userId, AsyncHttpResponseHandler responseHandler) throws ConnectionOpenException{
        if(mConnectionOpen)
            throw new ConnectionOpenException();

        mConnectionOpen = true;
        get(ServiceCalls.getProjectsOf(userId), null, responseHandler);

    }
    public static void createProject(Context context, HttpEntity data, AsyncHttpResponseHandler responseHandler) throws ConnectionOpenException{
        if(mConnectionOpen)
            throw new ConnectionOpenException();

        mConnectionOpen = true;

        post(context, ServiceCalls.REQ_NEW_PROJECT, "application/json", data, responseHandler);
    }
    public static void getDetailsOf(String projectId, AsyncHttpResponseHandler responseHandler) throws ConnectionOpenException{
        if(mConnectionOpen)
            throw new ConnectionOpenException();

        mConnectionOpen = true;

        get(ServiceCalls.getDetailsOf(projectId), null, responseHandler);
    }

    public static void delete(String projectId, AsyncHttpResponseHandler responseHandler) throws ConnectionOpenException{
        if(mConnectionOpen)
            throw new ConnectionOpenException();

        mConnectionOpen = true;

        client.delete(getAbsoluteUrl(ServiceCalls.delete(projectId)), responseHandler);
    }

    public static void update(Project aProject, Context aContext, AsyncHttpResponseHandler aResponseHandler) throws ConnectionOpenException{
        if(mConnectionOpen)
            throw new ConnectionOpenException();

        mConnectionOpen = true;


        put(aProject, aContext, aResponseHandler);
    }

    public static void getFilteredUsers(String userInput, AsyncHttpResponseHandler responseHandler) {
        get(ServiceCalls.getFilteredUsers(userInput), null, responseHandler);
    }

    public static void closeConnection() {
        mConnectionOpen = false;
        client.cancelAllRequests(true);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        client.setMaxRetriesAndTimeout(2, 4000); //messo qui perchè chiamato sempre
        return BASE_URL + relativeUrl;
    }

}
