package com.mcontin.projectmanager;

/**
 * Created by mattia on 22/06/16.
 */
public class ConnectionOpenException extends Exception {
    public ConnectionOpenException() {
        new ConnectionOpenException("A connection is already open!");
    }

    public ConnectionOpenException(String message) {
        super(message);
    }
}
