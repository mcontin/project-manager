package com.mcontin.projectmanager.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.mcontin.projectmanager.ConnectionOpenException;
import com.mcontin.projectmanager.models.DeletionListener;
import com.mcontin.projectmanager.models.EditProjectListener;
import com.mcontin.projectmanager.models.ErrorResponse;
import com.mcontin.projectmanager.models.Project;
import com.mcontin.projectmanager.R;
import com.mcontin.projectmanager.util.ConnectionManager;
import com.mcontin.projectmanager.views.ConfirmDeletionDialog;
import com.mcontin.projectmanager.views.SmartProgressDialog;
import com.mcontin.projectmanager.util.RestClient;

import org.json.JSONObject;

import java.io.IOException;

import cz.msebera.android.httpclient.Header;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProjectDetailsFragment extends Fragment implements DeletionListener {

//  --------------------STATIC FIELDS---------------------
    private static final String TAG = "FragmentProjectDetails";
    private static final String TAG_PROJECT = "project";
    private static final String TAG_CONFIRM_DELETE = "delete";
//  --------------------INTERFACE----------------------
    private TextView mTextCode;
    private TextView mTextName;
    private TextView mTextDescription;
    private TextView mTextBeginDate;
    private TextView mTextType;
    private TextView mTextCreator;
    private TextView mTextManager;
    private FloatingActionButton mFabDelete;
    private FloatingActionButton mFabEdit;
//  --------------------FIELDS-----------------------
    private String mProjectId;
    private Project mProject;
    private DeletionListener mInstance;
    private EditProjectListener mListener;

    public ProjectDetailsFragment() {
        // Required empty public constructor
    }

    public static ProjectDetailsFragment newInstance(String aProjectId) {
        ProjectDetailsFragment vFrag = new ProjectDetailsFragment();
        Bundle vArgs = new Bundle();
        vArgs.putString(TAG_PROJECT, aProjectId);
        vFrag.setArguments(vArgs);
        return vFrag;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof EditProjectListener)
            mListener = (EditProjectListener) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() != null) {
            mProjectId = getArguments().getString(TAG_PROJECT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vRootView = inflater.inflate(R.layout.fragment_project_details, container, false);

        mInstance = this;

        if(savedInstanceState != null) {
            mProjectId = savedInstanceState.getString(TAG_PROJECT);
        }

        mTextCode = (TextView) vRootView.findViewById(R.id.label_code);
        mTextName = (TextView) vRootView.findViewById(R.id.label_name);
        mTextDescription = (TextView) vRootView.findViewById(R.id.label_description);
        mTextBeginDate = (TextView) vRootView.findViewById(R.id.label_begin_date);
        mTextType = (TextView) vRootView.findViewById(R.id.label_type);
        mTextCreator = (TextView) vRootView.findViewById(R.id.label_creator);
        mTextManager = (TextView) vRootView.findViewById(R.id.label_manager);

        getProjectDetails();

        mFabDelete = (FloatingActionButton) vRootView.findViewById(R.id.fab_delete);
        mFabEdit = (FloatingActionButton) vRootView.findViewById(R.id.fab_edit);

        mFabDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConfirmDeletionDialog.newInstance(mInstance).show(getFragmentManager(), TAG_CONFIRM_DELETE);
            }
        });
        mFabEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.edit(mProject);
            }
        });

        return vRootView;
    }

    private void getProjectDetails() {
        if(ConnectionManager.isConnected(getContext())) {
            try {
                RestClient.getDetailsOf(mProjectId,
                        new JsonHttpResponseHandler() {
                            private ObjectMapper mapper;

                            @Override
                            public void onStart() {
                                // starting the request

                                mapper = new ObjectMapper();

                                SmartProgressDialog.show(getContext(), "Fetching project details...");
                            }

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                mProject = new Project();

                                try {
                                    mProject = mapper.readValue(response.toString(), Project.class);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                displayProjectData();
                            }

                            /**
                             * Called when the server times out or there is no project for the user
                             * @param statusCode 0 when timeout, 404 when nothing found
                             * @param throwable type of error (e.g. if statusCode is 404, this is
                             *                  "Not found"
                             * @param errorResponse if no project is found for the user, this
                             *                      contains the message "No project found for user"
                             */
                            @Override
                            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                                Log.e(TAG, "onFailure: Code: " + statusCode);
                                Log.e(TAG, "onFailure: Error: " + throwable.getMessage());

                                if (errorResponse != null) {
                                    //error thrown by the server
                                    Log.e(TAG, "onFailure: ResponseJson: " + errorResponse.toString());
                                    try {
                                        ErrorResponse error = mapper.readValue(errorResponse.toString(), ErrorResponse.class);
                                        if (getContext() != null) {
                                            Toast.makeText(getContext(), error.getDetail(), Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    return;
                                }

                                if (statusCode == 0 && getContext() != null)
                                    //timeout
                                    Toast.makeText(getContext(), "Connection error, server didn't respond", Toast.LENGTH_SHORT).show();

                                if (getFragmentManager() != null)
                                    getFragmentManager().popBackStackImmediate();
                            }

                            @Override
                            public void onFinish() {
                                //called after the response has been received
                                SmartProgressDialog.dismiss();
                                RestClient.closeConnection();
                            }

                        });
            } catch (ConnectionOpenException e) {
                e.printStackTrace();
                SmartProgressDialog.show(getContext(), "Fetching project details...");
            }
        } else {
            Toast.makeText(getContext(), "No connection", Toast.LENGTH_SHORT).show();
            //this pops the listfragment and not this one for some reason
//            getFragmentManager().popBackStackImmediate();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(TAG_PROJECT, mProjectId);
    }

    private void displayProjectData() {
        mTextCode.setText(mProject.getCode());
        mTextName.setText(mProject.getName());
        mTextDescription.setText(mProject.getDescription());
        mTextBeginDate.setText(mProject.getBeginDate().toString());
        mTextType.setText(mProject.getType());
        mTextCreator.setText(mProject.getCreatorId());
        mTextManager.setText(mProject.getManagerId());
    }

    @Override
    public void onConfirmDelete() {
        if(ConnectionManager.isConnected(getContext())) {
            try {
                RestClient.delete(mProjectId,
                        new AsyncHttpResponseHandler() {
                            private ObjectMapper mapper;

                            @Override
                            public void onStart() {
                                mapper = new ObjectMapper();
                                SmartProgressDialog.show(getContext(), "Deleting project...");
                            }

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                Log.i(TAG, "onSuccess: ");
                                if (responseBody != null)
                                    Log.i(TAG, "onSuccess: response: " + new String(responseBody));

                                if (getContext() != null)
                                    Toast.makeText(getContext(), "Project deleted", Toast.LENGTH_LONG).show();

                                if (getFragmentManager() != null) {
                                    onFinish();
                                    getFragmentManager().popBackStackImmediate();
                                }
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable errorResponse) {
                                Log.i(TAG, "onFailure: Code: " + statusCode);
                                Log.e(TAG, "onFailure: Error: ", errorResponse);

                                if (responseBody != null) {
                                    //error thrown by the server
                                    Log.i(TAG, "onFailure: ResponseString: " + new String(responseBody));
                                    try {
                                        ErrorResponse error = mapper.readValue(errorResponse.toString(), ErrorResponse.class);
                                        if (getContext() != null) {
                                            Toast.makeText(getContext(), error.getDetail(), Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    return;
                                }

                                if (getContext() != null && statusCode == 0)
                                    //timeout
                                    Toast.makeText(getContext(), "Connection error, server didn't respond", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onFinish() {
                                super.onFinish();
                                SmartProgressDialog.dismiss();
                                RestClient.closeConnection();
                            }

                        });
            } catch (ConnectionOpenException e) {
                e.printStackTrace();
                SmartProgressDialog.show(getContext(), "Deleting project...");
            }
        } else
            Toast.makeText(getContext(), "No connection", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        SmartProgressDialog.dismiss();
    }

}
