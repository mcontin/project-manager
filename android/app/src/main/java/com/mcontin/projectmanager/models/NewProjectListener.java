package com.mcontin.projectmanager.models;

/**
 * Created by mattia on 20/06/16.
 */
public interface NewProjectListener {
    void onNewClicked();
}
