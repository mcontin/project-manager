package com.mcontin.projectmanager.views;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.mcontin.projectmanager.fragments.EditProjectFragment;
import com.mcontin.projectmanager.models.EditProjectListener;
import com.mcontin.projectmanager.models.GetDetailsListener;
import com.mcontin.projectmanager.models.NewProjectListener;
import com.mcontin.projectmanager.R;
import com.mcontin.projectmanager.fragments.NewProjectFragment;
import com.mcontin.projectmanager.fragments.ProjectDetailsFragment;
import com.mcontin.projectmanager.fragments.ProjectsListFragment;
import com.mcontin.projectmanager.models.Project;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        NewProjectListener,
        GetDetailsListener,
        EditProjectListener {

//  ---------------------STATIC FIELDS-----------------------
    private final static String FRAGMENT_TAG = "frag";

//  ---------------------INTERFACE ELEMENTS-------------------------

//  ---------------------FIELDS-------------------------
    private FragmentManager mFragMan;
    private String mUserId = "mattia";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mFragMan = getSupportFragmentManager();

        if(mFragMan.findFragmentByTag(FRAGMENT_TAG) == null) {
            FragmentTransaction mTransaction = getSupportFragmentManager().beginTransaction();
            mTransaction.add(R.id.container, ProjectsListFragment.newInstance(mUserId), FRAGMENT_TAG);
            mTransaction.commit();
        }


//        ---------------------DEFAULT GUI SETUP------------------
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.addDrawerListener(toggle);
//        toggle.syncState();
//
//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onNewClicked() {
        FragmentTransaction vTransaction = mFragMan.beginTransaction();

        vTransaction.replace(R.id.container, NewProjectFragment.newInstance(mUserId), FRAGMENT_TAG);

        vTransaction.addToBackStack(FRAGMENT_TAG);
        vTransaction.commit();
    }

    @Override
    public void onDetailsRequested(String aProjectId) {
        FragmentTransaction vTransaction = mFragMan.beginTransaction();

        vTransaction.replace(R.id.container, ProjectDetailsFragment.newInstance(aProjectId), FRAGMENT_TAG);

        vTransaction.addToBackStack(FRAGMENT_TAG);
        vTransaction.commit();
    }

    @Override
    public void edit(Project aProject) {
        FragmentTransaction vTransaction = mFragMan.beginTransaction();

        vTransaction.replace(R.id.container, EditProjectFragment.newInstance(aProject), FRAGMENT_TAG);

        vTransaction.addToBackStack(FRAGMENT_TAG);
        vTransaction.commit();
    }
}
