package com.mcontin.projectmanager.models;

/**
 * Created by mattia on 29/06/16.
 */
public class User {
    private String id;

    public User() {

    }

    public User(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
