package com.mcontin.projectmanager.models;

/**
 * Created by mattia on 23/06/16.
 */
public interface DeletionListener {
    void onConfirmDelete();
}
