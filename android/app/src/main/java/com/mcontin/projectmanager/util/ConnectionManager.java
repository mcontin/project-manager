package com.mcontin.projectmanager.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by mattia on 24/06/16.
 */
public class ConnectionManager {

    public static boolean isConnected(Context aContext) {
        ConnectivityManager cm =
                (ConnectivityManager)aContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();

    }

}
