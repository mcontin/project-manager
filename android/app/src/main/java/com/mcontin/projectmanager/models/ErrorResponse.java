package com.mcontin.projectmanager.models;

/**
 * Created by mattia on 20/06/16.
 */
public class ErrorResponse {

    private String name;
    private String detail;

    public ErrorResponse(){
    }

    public ErrorResponse(String name, String detail) {
        this.name = name;
        this.detail = detail;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDetail() {
        return detail;
    }
    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        return obj instanceof ErrorResponse && equalTo((ErrorResponse) obj);
    }

    private boolean equalTo(ErrorResponse another) {
        return name.equals(another.name) && detail.equals(another.detail);
    }

}
