package com.mcontin.projectmanager.util;

/**
 * Created by mattia on 20/06/16.
 */
public class ServiceCalls {

    private static final String REQ_PROJECTS_OF_USER = "/api/projects/"; // + /:userId
    private static final String REQ_PROJECT_DETAILS = "/api/project/"; // + /:projectId
    public static final String REQ_NEW_PROJECT = "/api/projects/new"; // + post Json request
    public static final String REQ_GET_USERS = "/api/users/";

    public static String getProjectsOf(String userId) {
        return REQ_PROJECTS_OF_USER + userId;
    }

    public static String getDetailsOf(String projectId) {
        return REQ_PROJECT_DETAILS + projectId;
    }

    public static String edit(String projectId) {
        return getDetailsOf(projectId) + "/edit";
    }

    public static String delete(String projectId) {
        return getDetailsOf(projectId) + "/delete";
    }

    public static String getFilteredUsers(String partialUsername) {
        return REQ_GET_USERS + partialUsername;
    }
}
