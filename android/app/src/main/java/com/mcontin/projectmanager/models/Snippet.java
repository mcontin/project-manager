package com.mcontin.projectmanager.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mattia on 22/06/16.
 */
public class Snippet implements Parcelable{
    private String code;
    private String name;

    public Snippet() {
    }

    public Snippet(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object another) {
        if (this == another)
            return true;
        return another instanceof Snippet && equalTo((Snippet) another);
    }

    private boolean equalTo(Snippet another) {
        return code.equals(another.code);
    }

    @Override
    public String toString() {
        return "{code: " + code + ", name: " + name + "}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(code);
        parcel.writeString(name);
    }

    public static final Creator<Snippet> CREATOR = new Creator<Snippet>() {
        @Override
        public Snippet createFromParcel(Parcel in) {
            return new Snippet(in);
        }

        @Override
        public Snippet[] newArray(int size) {
            return new Snippet[size];
        }
    };


    protected Snippet(Parcel in) {
        code = in.readString();
        name = in.readString();
    }
}