package com.mcontin.projectmanager.fragments;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.mcontin.projectmanager.ConnectionOpenException;
import com.mcontin.projectmanager.models.ErrorResponse;
import com.mcontin.projectmanager.models.GetDetailsListener;
import com.mcontin.projectmanager.models.NewProjectListener;
import com.mcontin.projectmanager.adapters.ProjectsAdapter;
import com.mcontin.projectmanager.R;
import com.mcontin.projectmanager.models.Snippet;
import com.mcontin.projectmanager.util.ConnectionManager;
import com.mcontin.projectmanager.views.SmartProgressDialog;
import com.mcontin.projectmanager.util.RestClient;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Fragment to display the list of projects related to the user.
 * Everytime the fragment is shown, the list is updated for consistency with the server.
 */
public class ProjectsListFragment extends Fragment {

//  ---------------------STATIC FIELDS-----------------------

    private static final String TAG = "FragmentProjectsList";
    private static final String TAG_USER = "user";

//  ---------------------INTERFACE---------------------
    private RecyclerView mRecycler;
    private ProjectsAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;

//  ---------------------FIELDS------------------------
    private List<Snippet> mProjects;
    private String mUserId;
    private NewProjectListener mNewProjectListener;
    private GetDetailsListener mDetailsListener;

    public ProjectsListFragment() {
        // Required empty public constructor
    }

    /**
     * Create an instance of the fragment with the given userId, required to get the
     * list of projects
     */
    public static ProjectsListFragment newInstance(String userId) {
        ProjectsListFragment fragment = new ProjectsListFragment();
        Bundle args = new Bundle();
        args.putString(TAG_USER, userId);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * When the fragment is attached to the activity, set the activity as event listener
     * @param context the context of the activity it is attached to
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof NewProjectListener)
            mNewProjectListener = (NewProjectListener) context;
        if(context instanceof GetDetailsListener)
            mDetailsListener = (GetDetailsListener) context;
    }

    /**
     * Called when newly created, get the parameters from the bundle set at
     * {@link #newInstance(String)}
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mProjects = new ArrayList<>();
        mAdapter = new ProjectsAdapter(mProjects, mDetailsListener);

        if(getArguments() != null)
            mUserId = getArguments().getString(TAG_USER);

    }

    /**
     * Called when we need to setup the layout and when coming back from another fragment
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_projects_list, container, false);

        if(savedInstanceState != null) {
            mUserId = savedInstanceState.getString(TAG_USER);
        }

        setupLayoutManager();

        mRecycler = (RecyclerView) rootView.findViewById(R.id.projects_container);
        mRecycler.setLayoutManager(mLayoutManager);
        mRecycler.setAdapter(mAdapter);

        retrieveAllProjects();

        FloatingActionButton fabNew = (FloatingActionButton) rootView.findViewById(R.id.fab_new);
        fabNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fabClicked();
            }
        });

        FloatingActionButton fabRefresh = (FloatingActionButton) rootView.findViewById(R.id.fab_refresh);
        fabRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retrieveAllProjects();
            }
        });

        return rootView;
    }

    /**
     * Called when the fragment is not visible, covered by another fragment or the app goes in
     * background
     */
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        SmartProgressDialog.dismiss();
    }

    /**
     * Called when the fragment is destroyed (freeing up memory)
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        //when the fragment is destroyed, we want to close the connection since we cannot set the
        //view without fragment and restart the request when the fragment exists again
        RestClient.closeConnection();
    }

    /**
     * Called when the app goes in background or another fragment replaces this one
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(TAG_USER, mUserId);
    }

    /**
     * Decides whether to show the projects in 1 or 2 columns depending on the orientation of
     * the screen, landscape mode will refresh the projects
     */
    private void setupLayoutManager() {
        int cols = 1;
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            cols = 2;
        }
        mLayoutManager = new GridLayoutManager(getContext(), cols, GridLayoutManager.VERTICAL, false);
    }

    /**
     * Get all the projects related to the user
     */
    private void retrieveAllProjects() {
        if(ConnectionManager.isConnected(getContext())) {
            try {
                RestClient.getAllProjectsOf(
                        mUserId,
                        new JsonHttpResponseHandler() {
                            private ObjectMapper mapper;

                            /**
                             * Called before the request starts, show the progress dialog
                             * and instantiate the mapper for the json
                             */
                            @Override
                            public void onStart() {
                                mapper = new ObjectMapper();

                                SmartProgressDialog.show(getContext(), "Fetching projects...");
                            }

                            /**
                             * Called when the response is successful and is a list of projects
                             * @param statusCode code of the response, i.e. 200
                             * @param response array of projects
                             */
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                                List<Snippet> snippets = new ArrayList<>();

                                try {
                                    //map the response to the list of snippets
                                    snippets = mapper.readValue(response.toString(),
                                            new TypeReference<List<Snippet>>() {
                                            });
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                setProjects(snippets);
                            }

                            /**
                             * Called when the server times out or there is no project for the user
                             * @param statusCode 0 when timeout, 404 when nothing found
                             * @param throwable type of error (e.g. if statusCode is 404, this is
                             *                  "Not found"
                             * @param errorResponse if no project is found for the user, this
                             *                      contains the message "No project found for user"
                             */
                            @Override
                            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                                Log.e(TAG, "onFailure: Code: " + statusCode);
                                Log.e(TAG, "onFailure: Error: " + throwable.getMessage());

                                if (errorResponse != null) {
                                    //error thrown by the server
                                    Log.e(TAG, "onFailure: ResponseJson: " + errorResponse.toString());
                                    try {
                                        ErrorResponse error = mapper.readValue(errorResponse.toString(), ErrorResponse.class);
                                        if (getContext() != null) {
                                            Toast.makeText(getContext(), error.getDetail(), Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    return;
                                }

                                if (statusCode == 0 && getContext() != null)
                                    //timeout
                                    Toast.makeText(getContext(), "Connection error, server didn't respond", Toast.LENGTH_SHORT).show();
                            }

                            /**
                             * Called after the connection is closed
                             */
                            @Override
                            public void onFinish() {
                                SmartProgressDialog.dismiss();
                                RestClient.closeConnection();
                            }

                        });
            } catch (ConnectionOpenException e) {
                e.printStackTrace();
                //if the app is coming back up and the connection is still open, show the dialog
                SmartProgressDialog.show(getContext(), "Fetching projects...");
            }
        } else
            Toast.makeText(getContext(), "No connection", Toast.LENGTH_SHORT).show();

    }

    /**
     * Sets the current project list to the given list of projects
     */
    private void setProjects(List<Snippet> vSnippets) {
        mProjects.clear();
        mProjects.addAll(vSnippets);
        displayProjects();
    }

    /**
     * Refresh the recycler view to display the new list
     */
    private void displayProjects(){
        mAdapter.notifyDataSetChanged();
    }

    /**
     * The fab to create the new project is clicked
     */
    private void fabClicked() {
        mNewProjectListener.onNewClicked();
    }

}
