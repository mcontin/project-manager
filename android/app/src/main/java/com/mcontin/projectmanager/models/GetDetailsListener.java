package com.mcontin.projectmanager.models;

/**
 * Created by mattia on 22/06/16.
 */
public interface GetDetailsListener {
    void onDetailsRequested(String aProjectId);
}