package com.mcontin.projectmanager.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by mattia on 20/06/16.
 */
public class Project implements Parcelable{

    private String code;
    private String name;
    private String description;
    private Date creationDate;
    private Date beginDate;
    private String type;
    private String managerId;
    private Set<String> tools;
    private String creatorId;

    public Project(String code, String name, String description, long creationDate, long beginDate,
                   String type, String managerId, Set<String> tools, String creatorId) {
        this.code = code;
        this.name = name;
        this.description = description;
        this.creationDate = new Date(creationDate);
        this.beginDate = new Date(beginDate);
        this.type = type;
        this.managerId = managerId;
        this.tools = tools;
        this.creatorId = creatorId;
    }

    public Project(){

    }

    @Override
    public boolean equals(Object another) {
        if (this == another)
            return true;
        return another instanceof Project && equalTo((Project) another);
    }

    // A project is equal to another one when they have the same data
    private boolean equalTo(Project another) {
        return code.equals(another.code) && name.equals(another.name) &&
                description.equals(another.description) && creationDate.equals(another.creationDate) &&
                beginDate.equals(another.beginDate) && type.equals(another.type) &&
                managerId.equals(another.managerId) && tools.equals(another.tools) &&
                creatorId.equals(another.creatorId);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public Set<String> getTools() {
        return tools;
    }

    public void setTools(Set<String> tools) {
        this.tools = tools;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    protected Project(Parcel in) {
        code = in.readString();
        name = in.readString();
        description = in.readString();
        creationDate = new Date(in.readLong());
        beginDate = new Date(in.readLong());
        type = in.readString();
        managerId = in.readString();
        tools = new HashSet<>((List<String>) in.readSerializable());
        creatorId = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(code);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeLong(creationDate.getTime());
        dest.writeLong(beginDate.getTime());
        dest.writeString(type);
        dest.writeString(managerId);
        dest.writeSerializable(new ArrayList<>(tools));
        dest.writeString(creatorId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Project> CREATOR = new Creator<Project>() {
        @Override
        public Project createFromParcel(Parcel in) {
            return new Project(in);
        }

        @Override
        public Project[] newArray(int size) {
            return new Project[size];
        }
    };
}
