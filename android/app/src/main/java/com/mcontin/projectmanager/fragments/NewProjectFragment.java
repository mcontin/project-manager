package com.mcontin.projectmanager.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.mcontin.projectmanager.ConnectionOpenException;
import com.mcontin.projectmanager.models.ErrorResponse;
import com.mcontin.projectmanager.models.Project;
import com.mcontin.projectmanager.R;
import com.mcontin.projectmanager.models.User;
import com.mcontin.projectmanager.util.ConnectionManager;
import com.mcontin.projectmanager.views.DatePickerDialog;
import com.mcontin.projectmanager.views.SmartProgressDialog;
import com.mcontin.projectmanager.util.RestClient;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.entity.ByteArrayEntity;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.protocol.HTTP;

/**
 * Fragment that manages the creation of a project
 */
public class NewProjectFragment extends Fragment
        implements Validator.ValidationListener,
        android.app.DatePickerDialog.OnDateSetListener {

//    -----------------------STATIC FIELDS------------------------
    private static final String TAG = "FragmentNewProject";
    private static final String TAG_USER = "user";
    private static final String TAG_CODE = "code";
    private static final String TAG_NAME = "name";
    private static final String TAG_DESCRIPTION = "description";
    private static final String TAG_BEGIN_DATE = "begin";
    private static final String TAG_TYPE = "type";
    private static final String TAG_TOOLS = "tools";
    private static final String TAG_MANAGER = "manager";

//    -----------------------INTERFACE--------------------------
    @NotEmpty
    @Length(min = 4, message = "Minimum 4 characters")
    private EditText mInputCode;

    @NotEmpty
    private EditText mInputName;

    @NotEmpty
    private EditText mInputDescription;

    @NotEmpty
    private EditText mInputDate;

    private Spinner mTypeSpinner;

    @NotEmpty
    private AutoCompleteTextView mInputManager;

    private Button mButtonSubmit;
    private List<CheckBox> mCheckboxes;

//    -----------------------FIELDS-------------------------
    private String mUserId;
    private Validator mValidator;
    private NewProjectFragment mInstance;
    private String mCode;
    private String mName;
    private String mDescription;
    private Calendar mBeginDate;
    private String mType;
    private List<String> mTools;
    private String mManager;
    private List<String> mUsersList;

    /**
     * Create the new fragment with the id of the user that creates the project
     */
    public static NewProjectFragment newInstance(String userId) {
        NewProjectFragment fragment = new NewProjectFragment();
        Bundle args = new Bundle();
        args.putString(TAG_USER, userId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCheckboxes = new ArrayList<>();

        mValidator = new Validator(this);
        mValidator.setValidationListener(this);

        mUsersList = new ArrayList<>();
        mTools = new ArrayList<>();
        mBeginDate = Calendar.getInstance();
        mBeginDate.setTime(new Date());

        if(getArguments() != null) {
            mUserId = getArguments().getString(TAG_USER);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_new_project, container, false);

        mInstance = this;

        mInputCode = (EditText) rootView.findViewById(R.id.input_code);
        mInputName = (EditText) rootView.findViewById(R.id.input_name);
        mInputDescription = (EditText) rootView.findViewById(R.id.input_description);
        mInputDate = (EditText) rootView.findViewById(R.id.input_begin_date);
        mTypeSpinner = (Spinner) rootView.findViewById(R.id.spinner_type);
        mInputManager = (AutoCompleteTextView) rootView.findViewById(R.id.input_manager);
        mButtonSubmit = (Button) rootView.findViewById(R.id.submit_create);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.type_array, R.layout.spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mTypeSpinner.setAdapter(adapter);

        if(savedInstanceState != null) {
            mUserId = savedInstanceState.getString(TAG_USER);
            mCode = savedInstanceState.getString(TAG_CODE);
            mName = savedInstanceState.getString(TAG_NAME);
            mDescription = savedInstanceState.getString(TAG_DESCRIPTION);
            mBeginDate = (Calendar) savedInstanceState.getSerializable(TAG_BEGIN_DATE);
            mType = savedInstanceState.getString(TAG_TYPE);
            mTools = savedInstanceState.getStringArrayList(TAG_TOOLS);
            mManager = savedInstanceState.getString(TAG_MANAGER);

            //if the instance is restored, restore the views too
            mInputCode.setText(mCode);
            mInputName.setText(mName);
            mInputDescription.setText(mDescription);
            //format and set date
            SimpleDateFormat vDateFormat = new SimpleDateFormat("EEEE dd, MMMM yyyy");
            String vFormattedDate = vDateFormat.format(mBeginDate.getTime());
            mInputDate.setText(vFormattedDate);
            mTypeSpinner.setSelection(adapter.getPosition(mType));
            mInputManager.setText(mManager);
        }

        mInputCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            /**
             * Remove all spaces real-time
             */
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String vText = editable.toString();
                if(vText.contains(" ")) {
                    vText = vText.replaceAll("\\s","");
                    editable.clear();
                    editable.append(vText);
//                    mInputCode.setText("");
//                    mInputCode.append(vText);
                }
            }
        });

        mInputDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog.newInstance(mInstance).show(getFragmentManager(), "datePicker");
            }
        });

        mButtonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean contains = false;
                for(String user : mUsersList) {
                    if(mInputManager.getText().toString().equals(user))
                        contains = true;
                }
                if(contains)
                    mValidator.validate();
                else
                    new AlertDialog.Builder(getContext()).setMessage("The manager doesn't exist")
                            .show();
            }
        });

        GridLayout vToolsContainer = (GridLayout) rootView.findViewById(R.id.layout_tools);
        CheckBox vCheckBox;

        //for each view in the checkbox container
        for(int i = 0; i < vToolsContainer.getChildCount(); ++i) {

            //if the view is a checkbox
            if(vToolsContainer.getChildAt(i) instanceof CheckBox) {

                vCheckBox = (CheckBox) vToolsContainer.getChildAt(i);

                //when restoring the state, check the needed checkboxes
                if(mTools.contains(vCheckBox.getText().toString()))
                    vCheckBox.setChecked(true);
                vCheckBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onCheckboxClicked(view);
                    }
                });
                mCheckboxes.add(vCheckBox);

            }

        }

        mInputManager.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length() == 3)
                    getFilteredUsers(charSequence.toString());
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        return rootView;
    }

    /**
     * save input values to restore them later
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(TAG_USER, mUserId);
        outState.putString(TAG_CODE, mCode);
        outState.putString(TAG_NAME, mName);
        outState.putString(TAG_DESCRIPTION, mDescription);
        outState.putSerializable(TAG_BEGIN_DATE, mBeginDate);
        outState.putString(TAG_TYPE, mType);
        outState.putStringArrayList(TAG_TOOLS, (ArrayList<String>) mTools);
        outState.putString(TAG_MANAGER, mManager);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        SmartProgressDialog.dismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //when the fragment is destroyed, it's safer to just interrupt the connection
        RestClient.closeConnection();
    }

    /**
     * When all fields are validated, create a Project object, parse it to json and send it to
     * the server as HttpEntity
     */
    @Override
    public void onValidationSucceeded() {

        Collections.sort(mTools);

        Project vNewProject = new Project(mInputCode.getText().toString(),
                mInputName.getText().toString(),
                mInputDescription.getText().toString(),
                new Date().getTime(),
                mBeginDate.getTime().getTime(),
                mTypeSpinner.getSelectedItem().toString(),
                mInputManager.getText().toString(),
                new HashSet<>(mTools),
                mUserId);

        ObjectMapper mapper = new ObjectMapper();
        ByteArrayEntity vPayload;

        try {
            String jsonString = mapper.writeValueAsString(vNewProject);
            vPayload = new ByteArrayEntity(jsonString.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException | JsonProcessingException e) {
            e.printStackTrace();
            Toast.makeText(getContext(), "Error while parsing data, check the fields", Toast.LENGTH_SHORT).show();
            return;
        }

        vPayload.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

        createProject(vPayload);

    }

    /**
     * When a field is invalid, display the error
     */
    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for(ValidationError error : errors) {
            View view = error.getView();
            List<Rule> rules = error.getFailedRules();

            final String failureMessage = rules.get(0).getMessage(getContext());
            if (view instanceof EditText) {
                EditText failed = (EditText) view;
                failed.requestFocus();
                failed.setError(failureMessage);
            } else {
                Toast.makeText(getContext(), failureMessage, Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Called when the "Ok" button is clicked on the DatePickerDialog
     */
    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        mBeginDate = Calendar.getInstance();
        mBeginDate.set(year, month, day);

        SimpleDateFormat vDateFormat = new SimpleDateFormat("EEEE dd, MMMM yyyy");

        String vFormattedDate = vDateFormat.format(mBeginDate.getTime());

        mInputDate.setText(vFormattedDate);
    }

    /**
     * Checks connection availability and executes the call
     */
    private void createProject(HttpEntity aProjectData) {
        if(ConnectionManager.isConnected(getContext())) {
            try {
                RestClient.createProject(getContext(),
                        aProjectData,
                        new AsyncHttpResponseHandler() {
                            private ObjectMapper mapper;

                            /**
                             * Instantiate Json mapper and show progress dialog
                             */
                            @Override
                            public void onStart() {
                                mapper = new ObjectMapper();
                                SmartProgressDialog.show(getContext(), "Creating project...");
                            }

                            /**
                             * If the call is successful, display a toast for user feedback and
                             * close the fragment
                             */
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                if (getContext() != null)
                                    Toast.makeText(getContext(), "Project created!", Toast.LENGTH_LONG).show();

                                Log.e(TAG, "onSuccess: Code: " + statusCode);
                                if (responseBody != null)
                                    Log.e(TAG, "onSuccess: Response: " + new String(responseBody));

                                //force call onFinish, otherwise it isn't called after the pop
                                onFinish();
                                //if the project is created, pop the fragment and go back at list
                                getFragmentManager().popBackStackImmediate();
                            }

                            /**
                             * If the call fails, either display the error sent by the server or
                             * a generic timeout error
                             */
                            @Override
                            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable errorResponse) {
                                Log.i(TAG, "onFailure: Code: " + statusCode);
                                Log.e(TAG, "onFailure: Error: ", errorResponse);

                                if (responseBody != null) {
                                    //error thrown by the server
                                    Log.i(TAG, "onFailure: ResponseString: " + new String(responseBody));
                                    try {
                                        ErrorResponse error = mapper.readValue(errorResponse.toString(), ErrorResponse.class);
                                        if (getContext() != null) {
                                            Toast.makeText(getContext(), error.getDetail(), Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    return;
                                }

                                if (getContext() != null && statusCode == 0)
                                    //timeout
                                    Toast.makeText(getContext(), "Connection error, server didn't respond", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onFinish() {
                                SmartProgressDialog.dismiss();
                                RestClient.closeConnection();
                            }

                        });
            } catch (ConnectionOpenException e) {
                e.printStackTrace();
                SmartProgressDialog.show(getContext(), "Creating project...");
            }
        } else
            Toast.makeText(getContext(), "No connection", Toast.LENGTH_SHORT).show();
    }

    private void getFilteredUsers(String userInput) {
        if (ConnectionManager.isConnected(getContext())) {
            RestClient.getFilteredUsers(userInput,
                    new JsonHttpResponseHandler() {
                   private ObjectMapper mapper;

                /**
                 * Instantiate Json mapper and show progress dialog
                 */
                @Override
                public void onStart() {
                    mapper = new ObjectMapper();
                }

                /**
                 * Called when the response is successful and is a list of projects
                 *
                 * @param statusCode code of the response, i.e. 200
                 * @param response   array of projects
                 */
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    List<User> users = new ArrayList<>();
                    mUsersList.clear();

                    try {
                        users = mapper.readValue(response.toString(),
                                new TypeReference<List<User>>() {
                                });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    for (User user : users) {
                        mUsersList.add(user.getId());
                    }

                    setupUsersSuggestion();
                }

                /**
                 * If the call fails, either display the error sent by the server or
                 * a generic timeout error
                 */
                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    Log.e(TAG, "onFailure: Code: " + statusCode);
                    Log.e(TAG, "onFailure: Error: " + throwable.getMessage());

                    if (errorResponse != null) {
                        //error thrown by the server
                        Log.e(TAG, "onFailure: ResponseJson: " + errorResponse.toString());
                        try {
                            ErrorResponse error = mapper.readValue(errorResponse.toString(), ErrorResponse.class);
                            if (getContext() != null) {
                                Toast.makeText(getContext(), error.getDetail(), Toast.LENGTH_SHORT).show();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        return;
                    }

                    if (statusCode == 0 && getContext() != null)
                        //timeout
                        Toast.makeText(getContext(), "Couldn't get user suggestions, server didn't respond", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFinish() {
                }
            });

        }
    }

    /**
     * Everytime a checkbox is clicked, add the related tool to the list of tools
     * @param view the clicked checkbox
     */
    private void onCheckboxClicked(View view) {
        boolean vChecked = ((CheckBox) view).isChecked();
        String vText = ((CheckBox) view).getText().toString();

        if (vChecked) {
            mTools.add(vText);
        } else {
            mTools.remove(vText);
        }
    }

    private void setupUsersSuggestion() {
        // Get the string array
        final String[] users = mUsersList.toArray(new String[0]);
        // Create the adapter and set it to the AutoCompleteTextView
        ArrayAdapter<String> managerAdapter =
                new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, users);
        mInputManager.setAdapter(managerAdapter);
    }

}
