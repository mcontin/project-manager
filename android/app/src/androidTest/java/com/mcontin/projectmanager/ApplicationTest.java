package com.mcontin.projectmanager;

import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;

import com.mcontin.projectmanager.models.Project;
import com.mcontin.projectmanager.views.MainActivity;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.scrollToHolder;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withSpinnerText;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.HashSet;
import java.util.Random;

@RunWith(AndroidJUnit4.class)
public class ApplicationTest {

    private String mUserId;
    private Project projectToTest;

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void setup() {
        mUserId = "mattia";

    }

    /**
     * Espresso waits for the main thread to be idle, which means that this test will start
     * only after the "Loading..." dialog is dismissed.
     * Multiple tests were not written because for each test the app would restart, causing
     * the need to repeat code, so a single test was written and in it are executed methods
     * that test the application.
     */
    @Test
    public void test() {
        int i = 100;
        while(i-- > 0) {

            Random rand = new Random();
            int randomNum = rand.nextInt((1000000 - 1000) + 1) + 1000;
            projectToTest = new Project(Integer.toString(randomNum), "auto-name", "auto-description", 0, 0,
                    "Contest", "50", new HashSet<String>(), mUserId);

            clickFab();
            createProject();
            checkProject();
//            deleteProject();
//            checkDeletion();
            editProject();
            checkEdit();
        }
    }

    public void clickFab() {
        //get the list fragment
        ViewInteraction listFragment = onView(withId(R.id.list_fragment));

        //check if the fragment is shown
        listFragment.check(matches(isDisplayed()));

        //get the fab
        ViewInteraction fabNew = onView(withId(R.id.fab_new));

        //check if the fab is displayed
        fabNew.check(matches(isDisplayed()));

        //click the fab
        fabNew.perform(click());
    }

    public void createProject() {
        //get the project creation fragment
        ViewInteraction createProjectFragment = onView(withId(R.id.create_project));

        //check if it's shown
        createProjectFragment.check(matches(isDisplayed()));

        //insert random data in the form
        onView(withId(R.id.input_code))
                .perform(typeText(projectToTest.getCode()), closeSoftKeyboard());

        onView(withId(R.id.input_name))
                .perform(typeText(projectToTest.getName()), closeSoftKeyboard());

        onView(withId(R.id.input_description))
                .perform(typeText(projectToTest.getDescription()), closeSoftKeyboard());

        //click on the date input, the picker dialog will popup
        onView(withId(R.id.input_begin_date))
                .perform(click());
        //confirm today as begin date
        onView(withId(android.R.id.button1)).perform(click());

        //click on the spinner
        onView(withId(R.id.spinner_type))
                .perform(click());
        onData(allOf(is(instanceOf(String.class)), is(projectToTest.getType()))).perform(click());
        //check if the selected data equals to the clicked
        onView(withId(R.id.spinner_type)).check(matches(withSpinnerText(containsString(projectToTest.getType()))));

        onView(withId(R.id.checkbox_jira))
                .perform(click());
        onView(withId(R.id.checkbox_gitlab))
                .perform(click());

        onView(withId(R.id.input_manager))
                .perform(typeText(projectToTest.getManagerId()), closeSoftKeyboard());

        onView(withId(R.id.submit_create))
                .perform(click());
    }

    public void checkProject() {

        waitForDbAndRefresh();

        Matcher<RecyclerView.ViewHolder> matcher = MyCardMatcher.withCode(projectToTest.getCode());

        onView(withId(R.id.projects_container))
                .perform(scrollToHolder(matcher),
                        RecyclerViewActions.actionOnHolderItem(
                        matcher, click()));

        onView(withId(R.id.label_code))
                .check(matches(withText(projectToTest.getCode())));

        onView(withId(R.id.label_name))
                .check(matches(withText(projectToTest.getName())));

        onView(withId(R.id.label_description))
                .check(matches(withText(projectToTest.getDescription())));

        onView(withId(R.id.label_type))
                .check(matches(withText(projectToTest.getType())));

        onView(withId(R.id.label_creator))
                .check(matches(withText(projectToTest.getCreatorId())));

        onView(withId(R.id.label_manager))
                .check(matches(withText(projectToTest.getManagerId())));

        //after checking, go back to the project list
//        pressBack();
    }

    public void deleteProject() {
        //get the deletion fab
        ViewInteraction fabDelete = onView(withId(R.id.fab_delete));

        fabDelete.perform(click());

        //confirm today as begin date
        onView(withId(android.R.id.button1)).perform(click());

    }

    public void checkDeletion() {
        waitForDbAndRefresh();

        Matcher<RecyclerView.ViewHolder> matcher = MyCardMatcher.withCode(projectToTest.getCode());

        //this will result in an error because, if the project has been deleted, it will not be
        //found in the recycler view
        onView(withId(R.id.projects_container))
                .perform(scrollToHolder(matcher));
    }

    private void waitForDbAndRefresh() {
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //get the refresh fab
        ViewInteraction fabRefresh = onView(withId(R.id.fab_refresh));

        fabRefresh.perform(click());
    }

    public void editProject() {
        //get the edit fab
        ViewInteraction fabEdit = onView(withId(R.id.fab_edit));

        fabEdit.perform(click());

        projectToTest.setName("auto-name-mod");
        projectToTest.setDescription("auto-description-mod");

        onView(withId(R.id.input_edit_name))
                .perform(clearText(), typeText(projectToTest.getName()), closeSoftKeyboard());

        onView(withId(R.id.input_edit_description))
                .perform(clearText(), typeText(projectToTest.getDescription()), closeSoftKeyboard());

        onView(withId(R.id.submit_edit))
                .perform(scrollTo(), click());

        pressBack();
    }

    public void checkEdit() {
        checkProject();

        pressBack();
    }

}