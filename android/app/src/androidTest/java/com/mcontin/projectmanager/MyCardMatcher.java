package com.mcontin.projectmanager;

import android.support.test.espresso.matcher.BoundedMatcher;
import android.support.v7.widget.RecyclerView;

import com.mcontin.projectmanager.adapters.ProjectsAdapter;
import org.hamcrest.Description;
import org.hamcrest.Matcher;

/**
 * Created by mattia on 07/07/16.
 */
public class MyCardMatcher{

    public static Matcher<RecyclerView.ViewHolder> withCode(final String code) {
        return new BoundedMatcher<RecyclerView.ViewHolder, ProjectsAdapter.CardViewHolder>
                (ProjectsAdapter.CardViewHolder.class) {
            @Override
            protected boolean matchesSafely(ProjectsAdapter.CardViewHolder item) {
                return item.code.getText().toString().equalsIgnoreCase(code);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("view holder with code: " + code);
            }
        };
    }

}
