package mc.projectmanager.project.api;

import akka.NotUsed;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.api.transport.Method;
import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import static com.lightbend.lagom.javadsl.api.Service.*;

import java.util.concurrent.ExecutionException;

import org.pcollections.PSequence;

public interface ProjectService extends Service {

	ServiceCall<Project, NotUsed> addProject();
	
	ServiceCall<NotUsed, PSequence<ProjectSnippet>> getProjects(String userId);

	ServiceCall<NotUsed, Project> getProject(String projectId) throws InterruptedException, ExecutionException; 
	
	ServiceCall<Project, NotUsed> updateProject(String projectId);
	
	ServiceCall<NotUsed, NotUsed> deleteProject(String projectId);
	
	ServiceCall<NotUsed, NotUsed> closeProject(String projectId);
	
	@Override
	default Descriptor descriptor() {
		// @formatter:off
		return named("projectservice")
				.with(
				namedCall("/api/projects/new", this::addProject), //Ok, must check if a project with the same code already exists
				pathCall("/api/projects/:userId", this::getProjects), //Ok, must check if the user is authorized to see the projects
				pathCall("/api/project/:projectId", this::getProject),  //Ok, must check if the user is authorized to see the project
				restCall(Method.PUT, "/api/project/:projectId/edit", this::updateProject), //Ok, must check if the user is authorized to edit the project
				restCall(Method.DELETE, "/api/project/:projectId/delete", this::deleteProject), //Ok, must check if the user logged in has the right to delete the project
				restCall(Method.PUT, "/api/project/:projectId/close", this::closeProject)
				).withAutoAcl(true);
		// @formatter:on
	}
}
