package mc.projectmanager.project.api;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;
import com.lightbend.lagom.serialization.Jsonable;

@SuppressWarnings("serial")
@Immutable
@JsonDeserialize
public class ProjectSnippet implements Jsonable{

	private String code;
	private String name;
	
	public ProjectSnippet() {
		code = "";
		name = "";
	}

	@JsonCreator
	public ProjectSnippet(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public boolean equals(@Nullable Object another) {
		if (this == another)
			return true;
		return another instanceof ProjectSnippet && equalTo((ProjectSnippet) another);
	}

	// A snippet is equal to another one when they have the same code
	private boolean equalTo(ProjectSnippet another) {
		return code.equals(another.code);
	}

	@Override
	public int hashCode() {
		int h = 31;
		h = h * 17 + code.hashCode();
		h = h * 17 + name.hashCode();
		return h;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper("Project")
				.add("code", code)
				.add("name", name)
				.toString();
	}
}
