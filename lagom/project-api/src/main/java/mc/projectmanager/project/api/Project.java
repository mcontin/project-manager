package mc.projectmanager.project.api;

import java.util.Date;
import java.util.Optional;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import org.pcollections.PSet;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.lightbend.lagom.serialization.Jsonable;

@SuppressWarnings("serial")
@Immutable
@JsonDeserialize
public final class Project implements Jsonable {
	
	public final String code;
	public final String name;
	public final String description;
	public final Date creationDate;
	public final Date beginDate;
	public final String type;
	public final String managerId;
	public final PSet<String> tools;
	public final String creatorId;
	
	public static Project empty() {
		return new Project();
	}
	
	private Project(){
		code = null;
		name = null;
		description = null;
		creationDate = null;
		beginDate = null;
		type = null;
		managerId = null;
		tools = null;
		creatorId = null;
	}
	
//	public Project(String code, String name, String description,
//			Date beginDate, String type, String managerId,
//			PSet<String> tools, Optional<String> creatorId) {
//		
//		this(code, name, description, Optional.empty(), beginDate,
//				type, managerId, tools, creatorId);
//	}
//	
//	public Project(String code, String name, String description,
//			Date beginDate, String type, String managerId, 
//			PSet<String> tools) {
//		this(code, name, description, Optional.empty(), beginDate,
//				type, managerId, tools, Optional.empty());
//	}

	@JsonCreator
	public Project(String code, String name, String description, 
			Optional<Date> creationDate, Date beginDate, String type, 
			String managerId, PSet<String> tools, Optional<String> creatorId) {
		this.code = Preconditions.checkNotNull(code, "code");
		this.name = Preconditions.checkNotNull(name, "name");
		this.description = description;
		this.creationDate = creationDate.orElse(new Date());
		this.beginDate = Preconditions.checkNotNull(beginDate, "begin date");
		this.type = Preconditions.checkNotNull(type, "type");
		this.managerId = Preconditions.checkNotNull(managerId, "manager ID");
		this.tools = Preconditions.checkNotNull(tools, "tools");
		this.creatorId = creatorId.orElse("");
	}
	
	public boolean exists() {
		return (code != null && creationDate != null);
	}
	
	@Override
	public boolean equals(@Nullable Object another) {
		if (this == another)
			return true;
		return another instanceof Project && equalTo((Project) another);
	}

	// A project is equal to another one when they have the same code
	private boolean equalTo(Project another) {
		return code.equals(another.code);
	}

	@Override
	public int hashCode() {
		int h = 31;
		h = h * 17 + code.hashCode();
		h = h * 17 + name.hashCode();
		h = h * 17 + creationDate.hashCode();
		return h;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper("Project")
				.add("code", code)
				.add("name", name)
				.add("description", description)
				.add("creationDate", creationDate)
				.add("beginDate", beginDate)
				.add("type", type)
				.add("managerId", managerId)
				.add("tools", tools)
				.add("creatorId", creatorId)
				.toString();
	}
}
