organization in ThisBuild := "mc.projectmanager"

// the Scala version that will be used for cross-compiled libraries
scalaVersion in ThisBuild := "2.11.7"

lazy val userApi = project("user-api")
	.settings(
		version := "1.0-SNAPSHOT",
		libraryDependencies ++= Seq(
    		lagomJavadslApi,
    		lagomJavadslJackson
  		)
    )

lazy val userImpl = project("user-impl")
	.enablePlugins(LagomJava)
	.settings(
		version := "1.0-SNAPSHOT",
		libraryDependencies ++= Seq(
			lagomJavadslPersistence,
      		lagomJavadslPubSub,
			lagomJavadslTestKit
		)
	)
	.settings(lagomForkedTestSettings: _*)
	.dependsOn(userApi)

lazy val projectApi = project("project-api")
  	.settings(
    	version := "1.0-SNAPSHOT",
    	libraryDependencies ++= Seq(
    		lagomJavadslApi,
    		lagomJavadslJackson
    	)
  	)
lazy val projectImpl = project("project-impl")
	.enablePlugins(LagomJava)
	.settings(
    	version := "1.0-SNAPSHOT",
    	libraryDependencies ++= Seq(
	    	lagomJavadslPersistence,
	    	lagomJavadslPubSub,
	    	lagomJavadslTestKit
    	)
  	)
	.settings(lagomForkedTestSettings: _*)
	.dependsOn(projectApi/*, authApi*/)
/*
lazy val authApi = project("auth-api")
	.enablePlugins(LagomJava)
	.settings(
		version := "1.0-SNAPSHOT",
    	libraryDependencies ++= Seq(
    		lagomJavadslApi,
    		lagomJavadslJackson
    	)
  	)
lazy val authImpl = project("auth-impl")
	.enablePlugins(LagomJava)
	.settings(
		version := "1.0-SNAPSHOT",
		libraryDependencies ++= Seq(
			lagomJavadslPersistence,
      		lagomJavadslPubSub,
			lagomJavadslTestKit)
		)
	.settings(lagomForkedTestSettings: _*)
	.dependsOn(authApi)
	*/
//lazy val loadTestApi = project("load-test-api").settings(version := "1.0-SNAPSHOT",libraryDependencies += lagomJavadslApi)

//lazy val loadTestImpl = project("load-test-impl").enablePlugins(LagomJava).settings(version := "1.0-SNAPSHOT").dependsOn(loadTestApi, friendApi, activityStreamApi, chirpApi)

def project(id: String) = Project(id, base = file(id))
  .settings(eclipseSettings: _*)
  .settings(javacOptions in compile ++= Seq("-encoding", "UTF-8", "-source", "1.8", "-target", "1.8", "-Xlint:unchecked", "-Xlint:deprecation"))
  .settings(jacksonParameterNamesJavacSettings: _*) // applying it to every project even if not strictly needed.


// See https://github.com/FasterXML/jackson-module-parameter-names
lazy val jacksonParameterNamesJavacSettings = Seq(
  javacOptions in compile += "-parameters"
)

// configuration of sbteclipse
lazy val eclipseSettings = Seq(
  EclipseKeys.projectFlavor := EclipseProjectFlavor.Java,
  EclipseKeys.withBundledScalaContainers := false,
  EclipseKeys.createSrc := EclipseCreateSrc.Default + EclipseCreateSrc.Resource,
  EclipseKeys.eclipseOutput := Some(".target"),
  EclipseKeys.withSource := true,
  EclipseKeys.withJavadoc := true,
  // avoid some scala specific source directories
  unmanagedSourceDirectories in Compile := Seq((javaSource in Compile).value),
  unmanagedSourceDirectories in Test := Seq((javaSource in Test).value)
)

// do not delete database files on start
lagomCassandraCleanOnStart in ThisBuild := false

// set up information for where to publish our bundles to
// (see http://conductr.lightbend.com/docs/1.1.x/CreatingBundles#Publishing-bundles for more
// information)
//licenses in ThisBuild := Seq("Apache-2.0" -> url("http://www.apache.org/licenses/LICENSE-2.0"))

//bintrayVcsUrl in Bundle in ThisBuild := Some("https://github.com/lagom/activator-lagom-java-chirper")
//bintrayOrganization in Bundle in ThisBuild := Some("typesafe")
//bintrayReleaseOnPublish in Bundle in ThisBuild := true
