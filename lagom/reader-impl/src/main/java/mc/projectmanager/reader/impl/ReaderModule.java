package mc.projectmanager.reader.impl;

import com.google.inject.AbstractModule;
import com.lightbend.lagom.javadsl.server.ServiceGuiceSupport;

import mc.projectmanager.reader.api.ReaderService;

public class ReaderModule extends AbstractModule implements ServiceGuiceSupport {
  @Override
  protected void configure() {
    bindServices(serviceBinding(ReaderService.class, ReaderServiceImpl.class));
  }
}
