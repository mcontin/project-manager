package mc.projectmanager.reader.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.pcollections.PSequence;
import org.pcollections.TreePVector;
import com.datastax.driver.core.Row;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.api.transport.NotFound;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRef;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRegistry;
import com.lightbend.lagom.javadsl.persistence.cassandra.CassandraReadSide;
import com.lightbend.lagom.javadsl.persistence.cassandra.CassandraSession;

import akka.NotUsed;
import mc.projectmanager.project.api.Project;
import mc.projectmanager.project.impl.ProjectCommand;
import mc.projectmanager.project.impl.ProjectCommand.GetProject;
import mc.projectmanager.project.impl.ProjectEntity;
import mc.projectmanager.project.impl.ProjectEventProcessor;

import mc.projectmanager.reader.api.ProjectSnippet;
import mc.projectmanager.reader.api.ReaderService;

public class ReaderServiceImpl implements ReaderService {

	private final CassandraSession db;
	private PersistentEntityRegistry entityReg;
	
	@Inject
	public ReaderServiceImpl(PersistentEntityRegistry entityReg, CassandraSession db, CassandraReadSide readSide) {
		this.db = db;
		this.entityReg = entityReg;
		entityReg.register(ProjectEntity.class);
		readSide.register(ProjectEventProcessor.class);
		
		//used to select projects using managerId and creatorId
//		createIndexes();
	}
	/**
	 * Get the projects from the DB of which the current user is the manager or the creator.
	 * Since Cassandra doesn't support the OR operator this will be done with 2 separate queries.
	 * The resulting lists will then be combined.
	 */
	@Override
	public ServiceCall<NotUsed, PSequence<ProjectSnippet>> getProjects(String userId) {
		return req -> {
			//sequence of results of the queries
			PSequence<CompletionStage<PSequence<ProjectSnippet>>> results = TreePVector.empty();
			
			//query to get projects of which the current user is the creator
			CompletionStage<PSequence<ProjectSnippet>> resultCreator = 
					db.selectAll("SELECT code, name FROM projects WHERE creatorId = ?", userId)
					.thenApply(rows -> {
						List<ProjectSnippet> projects = rows.stream().map(this::mapProjectSnippet).collect(Collectors.toList());
						return TreePVector.from(projects);
					});
			//query to get projects of which the current user is the manager
			CompletionStage<PSequence<ProjectSnippet>> resultManager = 
					db.selectAll("SELECT code, name FROM projects WHERE managerId = ?", userId)
					.thenApply(rows -> {
						List<ProjectSnippet> projects = rows.stream().map(this::mapProjectSnippet).collect(Collectors.toList());
						return TreePVector.from(projects);
					});
			
			//concatenation of the two CompletionStage
			results = results.plus(resultCreator).plus(resultManager);
			
			CompletionStage<PSequence<ProjectSnippet>> combinedResults = null;
			for (CompletionStage<PSequence<ProjectSnippet>> projects : results) {
				//for each CompletionStage in results
				if (combinedResults == null) {
					//if they haven't been combined yet, get the first CompletionStage
					combinedResults = projects;
				} else {
					//check if the project to add isn't already in t
					if(!combinedResults.equals(projects))
						combinedResults = combinedResults.thenCombine(projects, 
								(a,b) -> {
									List<ProjectSnippet> c = new ArrayList<ProjectSnippet>();
									//adds all projects in first list to the third list
									c.addAll(a);
									for(ProjectSnippet project : b) {
										//if the third list is missing a project from the second list
										if (!c.contains(project)) {
											//add the missing project
											c.add(project);
										}
									}
									if(c.size() > 0)
									//return the third list as persistent sequence
										return TreePVector.from(c);
									throw new NotFound("No project was found for user " + userId);
								});
				}
			}
			
			return combinedResults;
			
		};
	}

	/**
	 * Returns the project requested in the URL. If no project is found this returns
	 * a project with all values set to null.
	 */
	@Override
	public ServiceCall<NotUsed, Project> getProject(String projectId) {
//		return req -> {
//			CompletionStage<Project> select = db
//					.selectOne("SELECT * FROM project WHERE code = ?", projectId)
//					.thenApply(row -> {
//						if (row.isPresent()) 
//							return row.map(this::mapProject).get();
//						throw new NotFound("Project " + projectId + " not found.");
//						});			
//			return select;
//		
//		};
		return request -> {
			return projectEntityRef(projectId).ask(new GetProject()).thenApply(reply -> {
				if(reply.project.isPresent())
					return reply.project.get();
				else 
					throw new NotFound("Project " + projectId + " not found.");
			});
		};
//		return request -> {
//			return projectEntityRef(projectId).ask(new GetProject()).thenApply(reply -> {
//				if (reply.project.isPresent())
//					return reply.project.get();
//				else
//					throw new NotFound("Project " + projectId + " not found");
//			});
//		};
	}
	
	/**
	 * Called for each row of the resulting query, creates and returns a project
	 * from the resulting rows.
	 * @param row the row that this method must be called upon
	 * @return a new Project object with the given parameters
	 */
	private ProjectSnippet mapProjectSnippet(Row row) {
		return new ProjectSnippet(
				row.getString("code"),
				row.getString("name"));
	}

	private PersistentEntityRef<ProjectCommand> projectEntityRef(String projectId) {
	    PersistentEntityRef<ProjectCommand> ref = entityReg.refFor(ProjectEntity.class, projectId);
	    return ref;
	}
	
}
