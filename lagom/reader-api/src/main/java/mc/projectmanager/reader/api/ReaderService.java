package mc.projectmanager.reader.api;

import static com.lightbend.lagom.javadsl.api.Service.named;
import static com.lightbend.lagom.javadsl.api.Service.pathCall;

import org.pcollections.PSequence;

import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;

import akka.NotUsed;

public interface ReaderService extends Service{
	

	/**
	 * Get projects which were created or are managed by the user
	 * associated with the given userId
	 */
	ServiceCall<NotUsed, PSequence<ProjectSnippet>> getProjects(String userId);

	/**
	 * Get the project identified by the parameter passed in the URL
	 */
	ServiceCall<NotUsed, mc.projectmanager.project.api.Project> getProject(String projectId);
	
	@Override
	default Descriptor descriptor() {
		// @formatter:off
		return null;
		// @formatter:on
	}
}
