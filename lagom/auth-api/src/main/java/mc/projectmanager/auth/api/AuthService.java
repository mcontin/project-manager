package mc.projectmanager.auth.api;

import static com.lightbend.lagom.javadsl.api.Service.named;
import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;

/**
 * Authentication and authorization service, all other services should
 * consume this before executing tasks
 */
public interface AuthService extends Service {

//	<Request, Response> ServiceCall<Request, Response> authenticate(
//			Function<Integer, ServiceCall<Request, Response>> serviceCall) ;

	ServiceCall<String, Integer> authorize(ServiceCall<Object, Object> serviceCall);
	
	@Override
	default Descriptor descriptor() {
		// @formatter:off
		return named("authservice");
		// @formatter:on
	}
	
}
