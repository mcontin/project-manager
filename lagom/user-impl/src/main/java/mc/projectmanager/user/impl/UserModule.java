package mc.projectmanager.user.impl;

import com.google.inject.AbstractModule;
import com.lightbend.lagom.javadsl.server.ServiceGuiceSupport;

import mc.projectmanager.user.api.UserService;

public class UserModule extends AbstractModule implements ServiceGuiceSupport {
  @Override
  protected void configure() {
    bindServices(serviceBinding(UserService.class, UserServiceImpl.class));
  }
}
