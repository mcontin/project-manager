/*
 * Copyright (C) 2016 Lightbend Inc. <http://www.lightbend.com>
 */
package mc.projectmanager.user.impl;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import org.pcollections.HashTreePMap;
import org.pcollections.HashTreePSet;
import org.pcollections.PSequence;
import org.pcollections.TreePVector;
import static java.util.concurrent.TimeUnit.SECONDS;

import java.util.ArrayList;
import java.util.Hashtable;

import com.datastax.driver.core.Row;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.api.transport.TransportErrorCode;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRegistry;
import com.lightbend.lagom.javadsl.persistence.cassandra.CassandraReadSide;
import com.lightbend.lagom.javadsl.persistence.cassandra.CassandraSession;
import com.typesafe.conductr.lib.java.Unit;
import com.lightbend.lagom.javadsl.api.transport.UnsupportedMediaType;
import com.lightbend.lagom.javadsl.api.deser.ExceptionMessage;

import akka.Done;
import akka.NotUsed;
import mc.projectmanager.user.api.UserService;
import play.Logger;
import play.Logger.ALogger;
import mc.projectmanager.user.api.LoginRequest;
import mc.projectmanager.user.api.User;

public class UserServiceImpl implements UserService {

	private final CassandraSession db;
	private final ALogger log = Logger.of(getClass());

	@Inject
	public UserServiceImpl(PersistentEntityRegistry persistentEntities, CassandraReadSide readSide,
			CassandraSession db) {

		this.db = db;

		createUsersTable();
	}

	@Override
	public ServiceCall<NotUsed, User> getUser(String userId) {
		return request -> {
			return null;
			// TODO if user found return
			// throw "not found"
		};
	}

	@Override
	public ServiceCall<User, NotUsed> createUser() {
		return request -> {
			CompletionStage<User> stage = db.selectOne("SELECT id FROM users WHERE id = ?", request.id)
					.thenApply(row -> {
						if (row.isPresent()) {
							ExceptionMessage exceptionMessage = new ExceptionMessage("DuplicateRecord",
									"User " + request.id + " already exists");
							throw new UnsupportedMediaType(TransportErrorCode.PolicyViolation, exceptionMessage);
						}
						return User.empty();
					});

			User existingUser = null;
			try {
				existingUser = stage.toCompletableFuture().get(3, SECONDS);
			} catch (InterruptedException | ExecutionException | TimeoutException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (existingUser != null && existingUser.isEmpty()) {
				CompletionStage<NotUsed> result = db
						.executeWrite("INSERT INTO users " + "(id) " + "VALUES (?)", request.id)
						.thenApply(done -> NotUsed.getInstance());
				return result;
			}

			ExceptionMessage exceptionMessage = new ExceptionMessage("DuplicateRecord",
					"User " + request.id + " already exists");
			throw new UnsupportedMediaType(TransportErrorCode.PolicyViolation, exceptionMessage);
		};
	}

	@Override
	public ServiceCall<NotUsed, PSequence<User>> getFilteredUsers(String userInput) {
		return request -> {

			final String ldapAdServer = "ldap://apitest2:636";
			final String ldapSearchBase = "ou=Persone,o=Engineering,c=IT";

			final String ldapUsername = "cn=manager,o=Engineering,c=it";
			final String ldapPassword = "terces";

			final String ldapAccountToLookup = "myOtherLdapUsername";

			Hashtable<String, Object> env = new Hashtable<String, Object>();
			
			env.put(Context.SECURITY_AUTHENTICATION, "simple");
			env.put(Context.SECURITY_PROTOCOL, "ssl");
			env.put(Context.SECURITY_PRINCIPAL, ldapUsername);
			env.put(Context.SECURITY_CREDENTIALS, ldapPassword);
			env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			env.put(Context.PROVIDER_URL, ldapAdServer);

			// ensures that objectSID attribute values
			// will be returned as a byte[] instead of a String
			env.put("java.naming.ldap.attributes.binary", "objectSID");

			// the following is helpful in debugging errors
			// env.put("com.sun.jndi.ldap.trace.ber", System.err);
			LdapContext ctx = null;
			try {
				ctx = new InitialLdapContext(env, null);
			} catch (NamingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			String searchFilter = "(cn=*" + userInput + "*)";

			SearchControls searchControls = new SearchControls();
			searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

			NamingEnumeration<SearchResult> results = null;
			try {
				results = ctx.search(ldapSearchBase, searchFilter, searchControls);
			} catch (NamingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			List<User> usersFromLdap = new ArrayList<>();
			SearchResult searchResult = null;
			User tempUser = null;
			while (results.hasMoreElements()) {
				searchResult = (SearchResult) results.nextElement();
				try {
					tempUser = new User(searchResult.getAttributes().get("gecos").get().toString());
				} catch (NamingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				usersFromLdap.add(tempUser);
			}

			CompletableFuture<PSequence<User>> completableFuture = new CompletableFuture<>();
			new Thread(() -> {
				 PSequence<User> resultUsers = TreePVector.from(usersFromLdap);
				/*
				 * we can manually "complete" a CompletableFuture!! this feature
				 * is not found with the classical Future interface
				 */
				completableFuture.complete(resultUsers);
			}, "CompFut1-Thread").start();

			return completableFuture;
//			
//			CompletionStage<PSequence<User>> resultCreator = db.selectAll("SELECT id FROM users").thenApply(rows -> {
//				List<User> users = rows.stream().map(this::mapUser).collect(Collectors.toList());
//				return TreePVector.from(users);
//			});
//			try {
//				PSequence<User> testA = resultCreator.toCompletableFuture().get();
//				/*
//				 * for(User user : test) { log.info(user.toString()); }
//				 */
//			} catch (InterruptedException | ExecutionException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			return resultCreator;
		};
	}

	@Override
	public ServiceCall<LoginRequest, NotUsed> login() {
		return request -> {
			return null;
			// if user found return ok
			// throw not found
		};
	}

	private User mapUser(Row row) {
		return new User(row.getString("id"));
	}

	private void createUsersTable() {
		// @formatter:off
		CompletionStage<Done> usersTable = db
				.executeCreateTable("CREATE TABLE IF NOT EXISTS users (" + "id text, " + "PRIMARY KEY (id) )");
		// @formatter:on
		usersTable.whenComplete((ok, err) -> {
			if (err != null) {
				log.error("Failed to create users table, due to: " + err.getMessage(), err);
			}
		});
	}

	// @Override
	// public ServiceCall<UserId, NotUsed> addFriend(String userId) {
	// return request -> {
	// return friendEntityRef(userId).ask(new AddFriend(request.friendId))
	// .thenApply(ack -> NotUsed.getInstance());
	// };
	// }
	//
	// @Override
	// public ServiceCall<NotUsed, PSequence<String>> getFollowers(String
	// userId) {
	// return req -> {
	// CompletionStage<PSequence<String>> result = db.selectAll("SELECT * FROM
	// follower WHERE userId = ?", userId)
	// .thenApply(rows -> {
	// List<String> followers = rows.stream().map(row ->
	// row.getString("followedBy")).collect(Collectors.toList());
	// return TreePVector.from(followers);
	// });
	// return result;
	// };
	// }

}
