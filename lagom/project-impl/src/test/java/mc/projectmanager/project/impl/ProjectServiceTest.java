/*
 * Copyright (C) 2016 Lightbend Inc. <http://www.lightbend.com>
 */
package mc.projectmanager.project.impl;

import static com.lightbend.lagom.javadsl.testkit.ServiceTest.*;
import static org.junit.Assert.*;

import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import com.lightbend.lagom.javadsl.testkit.ServiceTest.TestServer;

import akka.NotUsed;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.pcollections.HashTreePSet;
import org.pcollections.PSequence;
import mc.projectmanager.project.api.Project;
import mc.projectmanager.project.api.ProjectService;
import mc.projectmanager.project.api.ProjectSnippet;

public class ProjectServiceTest {

	private static TestServer server;

	@BeforeClass
	public static void setUp() {
		server = startServer(defaultSetup());
	}

	@AfterClass
	public static void tearDown() {
		server.stop();
		server = null;
	}

//	@Test
//	public void shouldSaveProjectToDatabase() throws Exception {
//		ProjectService projectService = server.client(ProjectService.class);
//		Set<String> lTools = new TreeSet<>();
//		lTools.add("tool1");
//		lTools.add("tool2");
//
//		Project request = new Project("code", "name", "description",
//				new Date(1), "type", "managerId", HashTreePSet.from(lTools), "creatorId");
//		
//		NotUsed response = projectService.addProject().invoke(request).toCompletableFuture().get();
//		
//		assertEquals(NotUsed.getInstance(), response);
//	}

	@Test
	public void shouldReadProjectsFromDatabase() throws Exception {
//		ProjectService projectService = server.client(ProjectService.class);
//		
//		PSequence<ProjectSnippet> response = projectService.getProjects("mc").invoke().toCompletableFuture().get();
//		
//		System.out.println(response.toString());
	}

//	@Test
//	public void shouldPublishShirpsToSubscribers() throws Exception {
//		ProjectService chirpService = server.client(ProjectService.class);
		// LiveChirpsRequest request = new
		// LiveChirpsRequest(TreePVector.<String>empty().plus("usr1").plus("usr2"));
		// Source<Project, ?> chirps1 =
		// chirpService.getLiveChirps().invoke(request).toCompletableFuture().get(3,
		// SECONDS);
		// Probe<Project> probe1 =
		// chirps1.runWith(TestSink.probe(server.system()),
		// server.materializer());
		// probe1.request(10);
		// Source<Project, ?> chirps2 =
		// chirpService.getLiveChirps().invoke(request).toCompletableFuture().get(3,
		// SECONDS);
		// Probe<Project> probe2 =
		// chirps2.runWith(TestSink.probe(server.system()),
		// server.materializer());
		// probe2.request(10);

		// Project chirp1 = new Project("usr1", "hello 1");
		// chirpService.addProject("usr1").invoke(chirp1).toCompletableFuture().get(3,
		// SECONDS);
		// probe1.expectNext(chirp1);
		// probe2.expectNext(chirp1);
		//
		// Project chirp2 = new Project("usr1", "hello 2");
		// chirpService.addProject("usr1").invoke(chirp2).toCompletableFuture().get(3,
		// SECONDS);
		// probe1.expectNext(chirp2);
		// probe2.expectNext(chirp2);
		//
		// Project chirp3 = new Project("usr2", "hello 3");
		// chirpService.addProject("usr2").invoke(chirp3).toCompletableFuture().get(3,
		// SECONDS);
		// probe1.expectNext(chirp3);
		// probe2.expectNext(chirp3);
		//
		// probe1.cancel();
		// probe2.cancel();
//	}
//
//	@Test
//	public void shouldIncludeSomeOldChirpsInLiveFeed() throws Exception {
//		ProjectService chirpService = server.client(ProjectService.class);

		// Project chirp1 = new Project("usr3", "hi 1");
		// chirpService.addProject("usr3").invoke(chirp1).toCompletableFuture().get(3,
		// SECONDS);
		//
		// Project chirp2 = new Project("usr4", "hi 2");
		// chirpService.addProject("usr4").invoke(chirp2).toCompletableFuture().get(3,
		// SECONDS);
		//
		// LiveChirpsRequest request = new
		// LiveChirpsRequest(TreePVector.<String>empty().plus("usr3").plus("usr4"));
		// Source<Project, ?> chirps =
		// chirpService.getLiveChirps().invoke(request).toCompletableFuture().get(3,
		// SECONDS);
		// Probe<Project> probe =
		// chirps.runWith(TestSink.probe(server.system()),
		// server.materializer());
		// probe.request(10);
		// probe.expectNextUnordered(chirp1, chirp2);
		//
		// Project chirp3 = new Project("usr4", "hi 3");
		// chirpService.addProject("usr4").invoke(chirp3).toCompletableFuture().get(3,
		// SECONDS);
		// probe.expectNext(chirp3);
		//
		// probe.cancel();
//	}
//
//	@Test
//	public void shouldRetrieveOldChirps() throws Exception {
//		ProjectService chirpService = server.client(ProjectService.class);

		// Project chirp1 = new Project("usr5", "msg 1");
		// chirpService.addProject("usr5").invoke(chirp1).toCompletableFuture().get(3,
		// SECONDS);
		//
		// Project chirp2 = new Project("usr6", "msg 2");
		// chirpService.addProject("usr6").invoke(chirp2).toCompletableFuture().get(3,
		// SECONDS);
		//
		// HistoricalChirpsRequest request = new
		// HistoricalChirpsRequest(Instant.now().minusSeconds(20),
		// TreePVector.<String>empty().plus("usr5").plus("usr6"));
		// Source<Project, ?> chirps =
		// chirpService.getHistoricalChirps().invoke(request).toCompletableFuture().get(3,
		// SECONDS);
		// Probe<Project> probe =
		// chirps.runWith(TestSink.probe(server.system()),
		// server.materializer());
		// probe.request(10);
		// probe.expectNextUnordered(chirp1, chirp2);
		// probe.expectComplete();
//	}

}
