/*
 * Copyright (C) 2016 Lightbend Inc. <http://www.lightbend.com>
 */
package mc.projectmanager.project.impl;

import java.util.Optional;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.lightbend.lagom.serialization.Jsonable;

import mc.projectmanager.project.api.Project;

@SuppressWarnings("serial")
@Immutable
@JsonDeserialize
public final class ProjectState implements Jsonable {

	public final Optional<Project> project;

	@JsonCreator
	public ProjectState(Optional<Project> project) {
		this.project = Preconditions.checkNotNull(project, "project");
	}

	@Override
	public boolean equals(@Nullable Object another) {
		if (this == another)
			return true;
		return another instanceof ProjectState && equalTo((ProjectState) another);
	}

	private boolean equalTo(ProjectState another) {
		return project.equals(another.project);
	}

	@Override
	public int hashCode() {
		int h = 31;
		h = h * 17 + project.hashCode();
		return h;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper("ProjectState").add("project", project).toString();
	}
}
