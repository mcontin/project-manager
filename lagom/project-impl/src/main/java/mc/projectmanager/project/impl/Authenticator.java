package mc.projectmanager.project.impl;

import static java.util.concurrent.CompletableFuture.completedFuture;

import java.util.Optional;
import java.util.concurrent.CompletionStage;

import javax.inject.Inject;

import com.lightbend.lagom.javadsl.api.transport.Forbidden;
import com.lightbend.lagom.javadsl.persistence.cassandra.CassandraSession;
import com.lightbend.lagom.javadsl.server.HeaderServiceCall;
import com.lightbend.lagom.javadsl.server.ServerServiceCall;

import akka.japi.Function;
import mc.projectmanager.project.impl.ProjectServiceImpl.UserStorage;

public class Authenticator {
	
	private final CassandraSession db = null;
	
	public <Request, Response> ServerServiceCall<Request, Response> authenticated(
			Function<Integer, ServerServiceCall<Request, Response>> serviceCall) {
		return HeaderServiceCall.composeAsync(requestHeader -> {

			UserStorage userStorage = new UserStorage() {

				@Override
				public CompletionStage<Optional<Integer>> lookupUser(String username) {
					// TODO Auto-generated method stub
					return completedFuture(Optional.empty());
				}
				
			};

			// First lookup user
			CompletionStage<Optional<Integer>> userLookup = requestHeader.principal()
					.map(principal -> userStorage.lookupUser(principal.getName()))
					.orElse(completedFuture(Optional.empty()));

			// Then, if it exists, apply it to the service call
			return userLookup.thenApply(maybeUser -> {
				if (maybeUser.isPresent()) {
					try {
						return serviceCall.apply(maybeUser.get());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				throw new Forbidden("User must be authenticated to access this service call");
			});
		});
	}
	
}
