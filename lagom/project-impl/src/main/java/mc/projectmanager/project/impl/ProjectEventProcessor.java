package mc.projectmanager.project.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletionStage;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.cassandra.CassandraReadSideProcessor;
import com.lightbend.lagom.javadsl.persistence.cassandra.CassandraSession;

import akka.Done;
import mc.projectmanager.project.impl.ProjectEvent.ProjectClosed;
import mc.projectmanager.project.impl.ProjectEvent.ProjectCreated;
import mc.projectmanager.project.impl.ProjectEvent.ProjectDeleted;
import mc.projectmanager.project.impl.ProjectEvent.ProjectEdited;

public class ProjectEventProcessor extends CassandraReadSideProcessor<ProjectEvent> {

	private PreparedStatement writeProject = null; // initialized in prepare
	private PreparedStatement updateProject = null; // initialized in prepare
	private PreparedStatement backupProject = null;
	private PreparedStatement deleteProject = null;
	private PreparedStatement closeProject = null;
	private PreparedStatement writeOffset = null; // initialized in prepare

	private void setWriteProject(PreparedStatement writeProject) {
		this.writeProject = writeProject;
	}

	private void setUpdateProject(PreparedStatement updateProject) {
		this.updateProject = updateProject;
	}
	
	private void setBackupProject(PreparedStatement backupProject) {
		this.backupProject = backupProject;
	}

	private void setDeleteProject(PreparedStatement deleteProject) {
		this.deleteProject = deleteProject;
	}

	private void setCloseProject(PreparedStatement closeProject) {
		this.closeProject = closeProject;
	}

	private void setWriteOffset(PreparedStatement writeOffset) {
		this.writeOffset = writeOffset;
	}

	@Override
	public AggregateEventTag<ProjectEvent> aggregateTag() {
		return ProjectEventTag.INSTANCE;
	}

	@Override
	public CompletionStage<Optional<UUID>> prepare(CassandraSession session) {
		return prepareCreateTables(session).thenCompose(a ->
	      prepareWriteProject(session).thenCompose(b ->
	      prepareUpdateProject(session).thenCompose(c ->
	      prepareBackupProject(session).thenCompose(d ->
	      prepareCloseProject(session).thenCompose(e ->
	      prepareDeleteProject(session).thenCompose(f ->
	      prepareWriteOffset(session).thenCompose(g ->
	      selectOffset(session))))))));
	}

	private CompletionStage<Done> prepareCreateTables(CassandraSession session) {
		// @formatter:off
		return session
				.executeCreateTable("CREATE TABLE IF NOT EXISTS projects (" + "code text, " + "name text, "
						+ "description text, " + "creationDate bigint, " + "beginDate bigint, " + "type text, "
						+ "managerId text, " + "tools set<text>, " + "creatorId text, " + "PRIMARY KEY (code) )")
				.thenCompose(a -> session.executeCreateTable("CREATE TABLE IF NOT EXISTS project_offset ("
						+ "partition int, offset timeuuid, " + "PRIMARY KEY (partition))"))
				.thenCompose(b -> session
						.executeCreateTable("CREATE INDEX IF NOT EXISTS project_manager ON projects (managerId);"))
				.thenCompose(c -> session
						.executeCreateTable("CREATE INDEX IF NOT EXISTS project_creator ON projects (creatorId);"))
				.thenCompose(d -> session.executeCreateTable("CREATE TABLE IF NOT EXISTS deleted_projects ("
						+ "code text, " + "name text, " + "description text, " + "creationDate bigint, "
						+ "beginDate bigint, " + "type text, " + "managerId text, " + "tools set<text>, "
						+ "creatorId text, " + "PRIMARY KEY (code) )"))
				.thenCompose(d -> session.executeCreateTable("CREATE TABLE IF NOT EXISTS closed_projects ("
						+ "code text, " + "name text, " + "description text, " + "creationDate bigint, "
						+ "beginDate bigint, " + "type text, " + "managerId text, " + "tools set<text>, "
						+ "creatorId text, " + "PRIMARY KEY (code) )"))
				;
		// @formatter:on
	}

	private CompletionStage<Done> prepareWriteProject(CassandraSession session) {
		return session.prepare("INSERT INTO projects "
				+ "(code, name, description, creationDate, "
				+ "beginDate, type, managerId, tools, creatorId) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)").thenApply(ps -> {
			setWriteProject(ps);
			return Done.getInstance();
		});
	}

	private CompletionStage<Done> prepareUpdateProject(CassandraSession session) {
		return session.prepare(
				"UPDATE projects "
						+ "SET name = ?, description = ?, "
						+ "beginDate = ?, type = ?, "
						+ "managerId = ?, tools = ? "
						+ "WHERE code = ?").thenApply(ps -> {
			setUpdateProject(ps);
			return Done.getInstance();
		});
	}

	private CompletionStage<Done> prepareBackupProject(CassandraSession session) {
		return session.prepare(
				"INSERT INTO deleted_projects "
						+ "(code, name, description, creationDate, "
						+ "beginDate, type, managerId, tools, creatorId) "
						+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)").thenApply(ps -> {
			setBackupProject(ps);
			return Done.getInstance();
		});
	}

	private CompletionStage<Done> prepareCloseProject(CassandraSession session) {
		return session.prepare(
				"INSERT INTO closed_projects "
						+ "(code, name, description, creationDate, "
						+ "beginDate, type, managerId, tools, creatorId) "
						+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)").thenApply(ps -> {
			setCloseProject(ps);
			return Done.getInstance();
		});
	}	
	
	private CompletionStage<Done> prepareDeleteProject(CassandraSession session) {
		return session.prepare(
				"DELETE FROM projects WHERE code = ?").thenApply(ps -> {
			setDeleteProject(ps);
			return Done.getInstance();
		});
	}

	private CompletionStage<Done> prepareWriteOffset(CassandraSession session) {
		return session.prepare("INSERT INTO project_offset (partition, offset) VALUES (1, ?)").thenApply(ps -> {
			setWriteOffset(ps);
			return Done.getInstance();
		});
	}

	private CompletionStage<Optional<UUID>> selectOffset(CassandraSession session) {
		return session.selectOne("SELECT offset FROM project_offset")
				.thenApply(optionalRow -> optionalRow.map(r -> r.getUUID("offset")));
	}

	@Override
	public EventHandlers defineEventHandlers(EventHandlersBuilder builder) {
		builder.setEventHandler(ProjectCreated.class, this::processProjectCreated);
		builder.setEventHandler(ProjectEdited.class, this::processProjectEdited);
		builder.setEventHandler(ProjectDeleted.class , this::processProjectDeleted);
		builder.setEventHandler(ProjectClosed.class , this::processProjectClosed);
		return builder.build();
	}

	private CompletionStage<List<BoundStatement>> processProjectCreated(ProjectCreated event, UUID offset) {
		BoundStatement bindWriteProject = writeProject.bind();
		bindWriteProject.setString("code", event.project.code);
		bindWriteProject.setString("name", event.project.name);
		bindWriteProject.setString("description", event.project.description);
		bindWriteProject.setLong("creationDate", event.project.creationDate.getTime());
		bindWriteProject.setLong("beginDate", event.project.beginDate.getTime());
		bindWriteProject.setString("type", event.project.type);
		bindWriteProject.setSet("tools", event.project.tools);
		bindWriteProject.setString("managerId", event.project.managerId);
		bindWriteProject.setString("creatorId", event.project.creatorId);

		BoundStatement bindWriteOffset = writeOffset.bind(offset);
		return completedStatements(Arrays.asList(bindWriteProject, bindWriteOffset));
	}

	private CompletionStage<List<BoundStatement>> processProjectEdited(ProjectEdited event, UUID offset) {
		BoundStatement bindUpdateProject = updateProject.bind();
		bindUpdateProject.setString("name", event.project.name);
		bindUpdateProject.setString("description", event.project.description);
		bindUpdateProject.setLong("beginDate", event.project.beginDate.getTime());
		bindUpdateProject.setString("type", event.project.type);
		bindUpdateProject.setSet("tools", event.project.tools);
		bindUpdateProject.setString("managerId", event.project.managerId);
		
		//required for the WHERE clause in the query
		bindUpdateProject.setString("code", event.project.code);

		BoundStatement bindWriteOffset = writeOffset.bind(offset);
		return completedStatements(Arrays.asList(bindUpdateProject, bindWriteOffset));
	}
	
	private CompletionStage<List<BoundStatement>> processProjectClosed(ProjectClosed event, UUID offset) {
		BoundStatement bindCloseProject = closeProject.bind();
		bindCloseProject.setString("code", event.project.code);
		bindCloseProject.setString("name", event.project.name);
		bindCloseProject.setString("description", event.project.description);
		bindCloseProject.setLong("creationDate", event.project.creationDate.getTime());
		bindCloseProject.setLong("beginDate", event.project.beginDate.getTime());
		bindCloseProject.setString("type", event.project.type);
		bindCloseProject.setSet("tools", event.project.tools);
		bindCloseProject.setString("creatorId", event.project.creatorId);
		bindCloseProject.setString("managerId", event.project.managerId);

		BoundStatement bindDeleteProject = deleteProject.bind(event.project.code);
		
		BoundStatement bindWriteOffset = writeOffset.bind(offset);
		return completedStatements(Arrays.asList(bindCloseProject, bindDeleteProject, bindWriteOffset));
	}

	private CompletionStage<List<BoundStatement>> processProjectDeleted(ProjectDeleted event, UUID offset) {
		BoundStatement bindBackupProject = backupProject.bind();
		bindBackupProject.setString("code", event.project.code);
		bindBackupProject.setString("name", event.project.name);
		bindBackupProject.setString("description", event.project.description);
		bindBackupProject.setLong("creationDate", event.project.creationDate.getTime());
		bindBackupProject.setLong("beginDate", event.project.beginDate.getTime());
		bindBackupProject.setString("type", event.project.type);
		bindBackupProject.setSet("tools", event.project.tools);
		bindBackupProject.setString("creatorId", event.project.creatorId);
		bindBackupProject.setString("managerId", event.project.managerId);

		BoundStatement bindDeleteProject = deleteProject.bind(event.project.code);
//		bindBackupProject.setString("code", event.project.code);
		
		BoundStatement bindWriteOffset = writeOffset.bind(offset);
		return completedStatements(Arrays.asList(bindBackupProject, bindDeleteProject, bindWriteOffset));
	}
}
