package mc.projectmanager.project.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.pcollections.PSequence;
import org.pcollections.TreePVector;
import com.datastax.driver.core.Row;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.api.deser.ExceptionMessage;
import com.lightbend.lagom.javadsl.api.transport.NotAcceptable;
import com.lightbend.lagom.javadsl.api.transport.NotFound;
import com.lightbend.lagom.javadsl.api.transport.TransportErrorCode;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRef;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRegistry;
import com.lightbend.lagom.javadsl.persistence.cassandra.CassandraReadSide;
import com.lightbend.lagom.javadsl.persistence.cassandra.CassandraSession;
import static java.util.concurrent.CompletableFuture.completedFuture;
import akka.NotUsed;
//import mc.projectmanager.auth.api.AuthService;
import mc.projectmanager.project.api.Project;
import mc.projectmanager.project.api.ProjectService;
import mc.projectmanager.project.api.ProjectSnippet;
import mc.projectmanager.project.impl.ProjectCommand.CloseProject;
import mc.projectmanager.project.impl.ProjectCommand.CreateProject;
import mc.projectmanager.project.impl.ProjectCommand.DeleteProject;
import mc.projectmanager.project.impl.ProjectCommand.EditProject;
import mc.projectmanager.project.impl.ProjectCommand.GetProject;
import play.Logger;
import play.Logger.ALogger;

/**
 * Service to manage projects, write them and read them when the user requires
 * to.
 * 
 * @author mattia
 *
 */
public class ProjectServiceImpl implements ProjectService {

	interface UserStorage {
		CompletionStage<Optional<Integer>> lookupUser(String username);
	}

	private final CassandraSession db;
	private final ALogger log = Logger.of(getClass());
	private PersistentEntityRegistry entityReg;
//	private AuthService authService;

	@Inject
	public ProjectServiceImpl(PersistentEntityRegistry entityReg, CassandraSession db, CassandraReadSide readSide /*, 
			AuthService authService*/) {
		this.db = db;
		this.entityReg = entityReg;
//		this.authService = authService;
		entityReg.register(ProjectEntity.class);
		readSide.register(ProjectEventProcessor.class);
		
		// createProjectsTable();
		// createDeletedProjectsTable();
		//// used to select projects using both managerId and creatorId
		// createIndexes();
	}

	// private void createProjectsTable() {
	// // @formatter:off
	// CompletionStage<Done> projectTable =
	// db.executeCreateTable("CREATE TABLE IF NOT EXISTS project ("
	// + "code text, "
	// + "name text, "
	// + "description text, "
	// + "creationDate bigint, "
	// + "beginDate bigint, "
	// + "type text, "
	// + "managerId text, "
	// + "tools set<text>, "
	// + "creatorId text, "
	// + "PRIMARY KEY (code) )");
	// // @formatter:on
	// projectTable.whenComplete((ok, err) -> {
	// if (err != null) {
	// log.error("Failed to create projects table, due to: " + err.getMessage(),
	// err);
	// }
	// });
	// }

	// private void createDeletedProjectsTable() {
	// // @formatter:off
	// CompletionStage<Done> deletedProjectsTable =
	// db.executeCreateTable("CREATE TABLE IF NOT EXISTS deleted_project ("
	// + "code text, "
	// + "name text, "
	// + "description text, "
	// + "creationDate bigint, "
	// + "beginDate bigint, "
	// + "type text, "
	// + "managerId text, "
	// + "tools set<text>, "
	// + "creatorId text, "
	// + "PRIMARY KEY (code) )");
	// // @formatter:on
	// deletedProjectsTable.whenComplete((ok, err) -> {
	// if (err != null) {
	// log.error("Failed to create deleted projects table, due to: " +
	// err.getMessage(), err);
	// }
	// });
	// }

	// private void createIndexes() {
	// CompletionStage<Done> managerIndex =
	// db.executeCreateTable("CREATE INDEX IF NOT EXISTS project_manager ON
	// projects (managerId);");
	// managerIndex.whenComplete((ok, err) -> {
	// if (err != null) {
	// log.error("Failed to create manager index, due to: " + err.getMessage(),
	// err);
	// }
	// });
	// log.info("created first index");
	// CompletionStage<Done> creatorIndex =
	// db.executeCreateTable("CREATE INDEX IF NOT EXISTS project_creator ON
	// projects (creatorId);");
	// creatorIndex.whenComplete((ok, err) -> {
	// if (err != null) {
	// log.error("Failed to create creator index, due to: " + err.getMessage(),
	// err);
	// }
	// });
	// log.info("created second index");
	// }

	/**
	 * Adds the project sent with Json format on the header of the request to
	 * the database
	 */
	@Override
	public ServiceCall<Project, NotUsed> addProject() {
		// return project -> {
		// if(!projectExists(project.code)){
		// CompletionStage<NotUsed> result = db
		// .executeWrite("INSERT INTO project "
		// + "(code, name, description, creationDate, "
		// + "beginDate, type, managerId, tools, creatorId) "
		// + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
		// project.code,
		// project.name,
		// project.description,
		// project.creationDate.getTime(),
		// project.beginDate.getTime(),
		// project.type,
		// project.managerId,
		// project.tools,
		// project.creatorId)
		// .thenApply(done -> NotUsed.getInstance());
		// return result;
		// }
		//
		// ExceptionMessage exceptionMessage = new
		// ExceptionMessage("DuplicateRecord", "Project " + project.code + "
		// already exists");
		// throw new UnsupportedMediaType(TransportErrorCode.PolicyViolation,
		// exceptionMessage);
		// };
		return project -> {
			return projectEntityRef(project.code).ask(new GetProject()).thenCompose((reply) -> {
				if (reply.project.isPresent()) {
					ExceptionMessage exceptionMessage = new ExceptionMessage("ProjectExists",
							"Project with code " + project.code + " already exists!");
					throw new NotAcceptable(TransportErrorCode.PolicyViolation, exceptionMessage);
				}
				return projectEntityRef(project.code).ask(new CreateProject(project))
						.thenApply(ack -> NotUsed.getInstance());
			});
		};
	}

	/**
	 * Get the projects from the DB of which the current user is the manager or
	 * the creator. Since Cassandra doesn't support the OR operator this will be
	 * done with 2 separate queries. The resulting lists will then be combined.
	 */
	@Override
	public ServiceCall<NotUsed, PSequence<ProjectSnippet>> getProjects(String userId) {
		return req -> {
			// sequence of results of the queries
			PSequence<CompletionStage<PSequence<ProjectSnippet>>> results = TreePVector.empty();

			// query to get projects of which the current user is the creator
			CompletionStage<PSequence<ProjectSnippet>> resultCreator = db
					.selectAll("SELECT code, name FROM projects WHERE creatorId = ?", userId).thenApply(rows -> {
						List<ProjectSnippet> projects = rows.stream().map(this::mapProjectSnippet)
								.collect(Collectors.toList());
						return TreePVector.from(projects);
					});
			// query to get projects of which the current user is the manager
			CompletionStage<PSequence<ProjectSnippet>> resultManager = db
					.selectAll("SELECT code, name FROM projects WHERE managerId = ?", userId).thenApply(rows -> {
						List<ProjectSnippet> projects = rows.stream().map(this::mapProjectSnippet)
								.collect(Collectors.toList());
						return TreePVector.from(projects);
					});

			// concatenation of the two CompletionStage
			results = results.plus(resultCreator).plus(resultManager);

			CompletionStage<PSequence<ProjectSnippet>> combinedResults = null;
			for (CompletionStage<PSequence<ProjectSnippet>> projects : results) {
				// for each CompletionStage in results
				if (combinedResults == null) {
					// if they haven't been combined yet, get the first
					// CompletionStage
					combinedResults = projects;
				} else {
					// check if the project to add isn't already in t
					if (!combinedResults.equals(projects))
						combinedResults = combinedResults.thenCombine(projects, (a, b) -> {
							List<ProjectSnippet> c = new ArrayList<ProjectSnippet>();
							// adds all projects in first list to the third list
							c.addAll(a);
							for (ProjectSnippet project : b) {
								// if the third list is missing a project from
								// the second list
								if (!c.contains(project)) {
									// add the missing project
									c.add(project);
								}
							}
							if (c.size() > 0) {
								// return the third list as persistent sequence
								return TreePVector.from(c);
							}
							throw new NotFound("No project was found for user " + userId);
						});
				}
			}

			return combinedResults;

		};
	}

//	public ServiceCall<String, Integer> sayHello() {
//		return authService.authorize(authId -> completedFuture("Hello " + authId));
//	}

	/**
	 * Called for each row of the resulting query, creates and returns a project
	 * from the resulting rows.
	 * 
	 * @param row
	 *            the row that this method must be called upon
	 * @return a new Project object with the given parameters
	 */
	// private Project mapProject(Row row) {
	// Set<String> set = row.getSet("tools", String.class);
	// return new Project(
	// row.getString("code"),
	// row.getString("name"),
	// row.getString("description"),
	// Optional.of(new Date(row.getLong("creationDate"))),
	// new Date(row.getLong("beginDate")),
	// row.getString("type"),
	// row.getString("managerId"),
	// HashTreePSet.from(set),
	// row.getString("creatorId"));
	// }

	/**
	 * Called for each row of the resulting query, creates and returns a project
	 * from the resulting rows.
	 * 
	 * @param row
	 *            the row that this method must be called upon
	 * @return a new Project object with the given parameters
	 */
	private ProjectSnippet mapProjectSnippet(Row row) {
		return new ProjectSnippet(row.getString("code"), row.getString("name"));
	}

	/**
	 * Returns the project requested in the URL. If no project is found this
	 * returns a project with all values set to null.
	 */
	public ServiceCall<NotUsed, Project> getProject(String projectId) {
		// return req -> {
		// CompletionStage<Project> select = db
		// .selectOne("SELECT * FROM project WHERE code = ?", projectId)
		// .thenApply(row -> {
		// if (row.isPresent())
		// return row.map(this::mapProject).get();
		// throw new NotFound("Project " + projectId + " not found.");
		// });
		// return select;
		//
		// };
		return request -> {
			return projectEntityRef(projectId).ask(new GetProject()).thenApply(reply -> {
				if (reply.project.isPresent())
					return reply.project.get();
				else
					throw new NotFound("Project " + projectId + " not found");
			});
		};
	}

	/**
	 * Updates the project identified by the ID passed in the URL with the data
	 * passed in the request body
	 * 
	 * @param projectId
	 *            identifier for the project that needs to be updated
	 */
	@Override
	public ServiceCall<Project, NotUsed> updateProject(String projectId) {
		// return req -> {
		// if(projectExists(projectId)) {
		// CompletionStage<NotUsed> update = db
		// .executeWrite("UPDATE project "
		// + "SET name = ?, description = ?, "
		// + "beginDate = ?, type = ?, "
		// + "managerId = ?, tools = ? "
		// + "WHERE code = ?",
		// req.name, req.description,
		// req.beginDate.getTime(), req.type,
		// req.managerId, req.tools,
		// projectId)
		// .thenApply(done -> NotUsed.getInstance());
		// return update;
		// }
		// throw new NotFound("Requested project to edit '" + projectId + "'
		// does not exist.");
		// };
		return project -> {
			return projectEntityRef(projectId).ask(new GetProject()).thenCompose((reply) -> {
				if (reply.project.isPresent()) {
					if (!reply.project.get().code.equals(project.code)) {
						ExceptionMessage exceptionMessage = new ExceptionMessage("UnacceptedValue",
								"Cannot change the code!");
						throw new NotAcceptable(TransportErrorCode.PolicyViolation, exceptionMessage);
					}
					if (!reply.project.get().creationDate.equals(project.creationDate)) {
						ExceptionMessage exceptionMessage = new ExceptionMessage("UnacceptedValue",
								"Cannot change the creation date!");
						throw new NotAcceptable(TransportErrorCode.PolicyViolation, exceptionMessage);
					}
					if (!reply.project.get().creatorId.equals(project.creatorId)) {
						ExceptionMessage exceptionMessage = new ExceptionMessage("UnacceptedValue",
								"Cannot change the creator!");
						throw new NotAcceptable(TransportErrorCode.PolicyViolation, exceptionMessage);
					}
					return projectEntityRef(projectId).ask(new EditProject(project))
							.thenApply(ack -> NotUsed.getInstance());
				}
				throw new NotFound("Project " + projectId + " not found");
			});
		};
	}

	/**
	 * Check if the project exists
	 */
	// private boolean projectExists(String projectId) {
	// return getAProject(projectId).exists();
	// }

	// private Project getAProject(String projectId) {
	// CompletionStage<Project> stage =
	// db.selectOne("SELECT * FROM projects WHERE code = ?", projectId)
	// .thenApply(row -> {
	// if (row.isPresent())
	// return row.map(this::mapProject).get();
	// return Project.empty();
	// });
	// Project projectToReturn = Project.empty();
	//
	// try {
	// projectToReturn = stage.toCompletableFuture().get();
	// } catch (InterruptedException e) {
	// e.printStackTrace();
	// } catch (ExecutionException e) {
	// e.printStackTrace();
	// }
	//
	// return projectToReturn;
	// }

	/**
	 * Delete a project logically
	 */
	@Override
	public ServiceCall<NotUsed, NotUsed> deleteProject(String projectId) {
		// return req -> {
		// if(projectExists(projectId)) {
		// Project projectToDelete = getAProject(projectId);
		//
		// CompletionStage<Done> putInDeletedTable = db
		// .executeWrite("INSERT INTO deleted_project "
		// + "(code, name, description, creationDate, "
		// + "beginDate, type, managerId, tools, creatorId) "
		// + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
		// projectToDelete.code,
		// projectToDelete.name,
		// projectToDelete.description,
		// projectToDelete.creationDate.getTime(),
		// projectToDelete.beginDate.getTime(),
		// projectToDelete.type,
		// projectToDelete.managerId,
		// projectToDelete.tools,
		// projectToDelete.creatorId);
		// putInDeletedTable.whenComplete((ok, err) -> {
		// if (err != null) {
		// log.error("Failed to add project to deleted table, due to: " +
		// err.getMessage(), err);
		// }
		// });
		//
		// CompletionStage<NotUsed> deletion = db
		// .executeWrite("DELETE FROM projects WHERE code = ?", projectId)
		// .thenApply(done -> NotUsed.getInstance());
		// return deletion;
		// }
		// throw new NotFound("Requested project to delete '" + projectId + "'
		// does not exist.");
		// };
		return req -> {
			return projectEntityRef(projectId).ask(new GetProject()).thenCompose((reply) -> {
				if (reply.project.isPresent())
					return projectEntityRef(projectId).ask(new DeleteProject(projectId))
							.thenApply(ack -> NotUsed.getInstance());
				throw new NotFound("Project " + projectId + " not found.");
			});
		};
	}

	@Override
	public ServiceCall<NotUsed, NotUsed> closeProject(String projectId) {
		return req -> {
			return projectEntityRef(projectId).ask(new GetProject()).thenCompose(reply -> {
				if (!reply.project.isPresent()) {
					throw new NotFound("Project " + projectId + " not found.");
				}
				return projectEntityRef(projectId)
						.ask(new CloseProject(projectId))
						.thenApply(ack -> NotUsed.getInstance());
			});
		};
	}

	private PersistentEntityRef<ProjectCommand> projectEntityRef(String projectId) {
		PersistentEntityRef<ProjectCommand> ref = entityReg.refFor(ProjectEntity.class, projectId);
		return ref;
	}

}
