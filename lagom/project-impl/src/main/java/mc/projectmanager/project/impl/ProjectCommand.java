package mc.projectmanager.project.impl;

import java.util.Optional;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.lightbend.lagom.javadsl.persistence.PersistentEntity;
import com.lightbend.lagom.serialization.Jsonable;

import akka.Done;
import mc.projectmanager.project.api.Project;

/**
 * TODO: - add project - edit project - delete project
 * 
 * @author mattia
 *
 */
public interface ProjectCommand extends Jsonable {

	@SuppressWarnings("serial")
	@Immutable
	@JsonDeserialize
	public final class CreateProject implements ProjectCommand, PersistentEntity.ReplyType<Done> {
		public final Project project;

		@JsonCreator
		public CreateProject(Project project) {
			this.project = Preconditions.checkNotNull(project, "project");
		}

		@Override
		public boolean equals(@Nullable Object another) {
			if (this == another)
				return true;
			return another instanceof CreateProject && equalTo((CreateProject) another);
		}

		private boolean equalTo(CreateProject another) {
			return project.equals(another.project);
		}

		@Override
		public int hashCode() {
			int h = 31;
			h = h * 17 + project.hashCode();
			return h;
		}

		@Override
		public String toString() {
			return MoreObjects.toStringHelper("CreateProject").add("project", project).toString();
		}
	}
	
	@SuppressWarnings("serial")
	@Immutable
	@JsonDeserialize
	public final class GetProject implements ProjectCommand, PersistentEntity.ReplyType<GetProjectReply> {

		@Override
		public boolean equals(@Nullable Object another) {
			return another instanceof GetProject;
		}

		@Override
		public int hashCode() {
			return 2053226012;
		}

		@Override
		public String toString() {
			return "GetUser{}";
		}
	}

	@SuppressWarnings("serial")
	@Immutable
	@JsonDeserialize
	public final class GetProjectReply implements Jsonable {
		public final Optional<Project> project;

		@JsonCreator
		public GetProjectReply(Optional<Project> project) {
			this.project = Preconditions.checkNotNull(project, "project");
		}

		@Override
		public boolean equals(@Nullable Object another) {
			if (this == another)
				return true;
			return another instanceof GetProjectReply && equalTo((GetProjectReply) another);
		}

		private boolean equalTo(GetProjectReply another) {
			return project.equals(another.project);
		}

		@Override
		public int hashCode() {
			int h = 31;
			h = h * 17 + project.hashCode();
			return h;
		}

		@Override
		public String toString() {
			return MoreObjects.toStringHelper("GetUserReply").add("project", project).toString();
		}
	}

	@SuppressWarnings("serial")
	@Immutable
	@JsonDeserialize
	public final class EditProject implements ProjectCommand, PersistentEntity.ReplyType<Done> {
		public final Project project;

		@JsonCreator
		public EditProject(Project project) {
			this.project = Preconditions.checkNotNull(project, "project");
		}

		@Override
		public boolean equals(@Nullable Object another) {
			if (this == another)
				return true;
			return another instanceof EditProject && equalTo((EditProject) another);
		}

		private boolean equalTo(EditProject another) {
			return project.equals(another.project);
		}

		@Override
		public int hashCode() {
			int h = 31;
			h = h * 17 + project.hashCode();
			return h;
		}

		@Override
		public String toString() {
			return MoreObjects.toStringHelper("EditProject").add("project", project).toString();
		}
	}

	@SuppressWarnings("serial")
	@Immutable
	@JsonDeserialize
	public final class DeleteProject implements ProjectCommand, PersistentEntity.ReplyType<Done> {
		public final String projectId;

		@JsonCreator
		public DeleteProject(String projectId) {
			this.projectId = Preconditions.checkNotNull(projectId, "projectId");
		}

		@Override
		public boolean equals(@Nullable Object another) {
			if (this == another)
				return true;
			return another instanceof DeleteProject && equalTo((DeleteProject) another);
		}

		private boolean equalTo(DeleteProject another) {
			return projectId.equals(another.projectId);
		}

		@Override
		public int hashCode() {
			int h = 31;
			h = h * 17 + projectId.hashCode();
			return h;
		}

		@Override
		public String toString() {
			return MoreObjects.toStringHelper("DeleteProject").add("projectId", projectId).toString();
		}
	}

	@SuppressWarnings("serial")
	@Immutable
	@JsonDeserialize
	public final class CloseProject implements ProjectCommand, PersistentEntity.ReplyType<Done> {
		public final String projectId;

		@JsonCreator
		public CloseProject(String projectId) {
			this.projectId = Preconditions.checkNotNull(projectId, "projectId");
		}

		@Override
		public boolean equals(@Nullable Object another) {
			if (this == another)
				return true;
			return another instanceof CloseProject && equalTo((CloseProject) another);
		}

		private boolean equalTo(CloseProject another) {
			return projectId.equals(another.projectId);
		}

		@Override
		public int hashCode() {
			int h = 31;
			h = h * 17 + projectId.hashCode();
			return h;
		}

		@Override
		public String toString() {
			return MoreObjects.toStringHelper("CloseProject").add("projectId", projectId).toString();
		}
	}
}
