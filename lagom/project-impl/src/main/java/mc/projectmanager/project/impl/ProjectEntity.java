package mc.projectmanager.project.impl;

import java.util.Optional;
import java.util.function.Consumer;

import com.lightbend.lagom.javadsl.api.transport.NotFound;
import com.lightbend.lagom.javadsl.persistence.PersistentEntity;

import akka.Done;
import mc.projectmanager.project.api.Project;
import mc.projectmanager.project.impl.ProjectCommand.CloseProject;
import mc.projectmanager.project.impl.ProjectCommand.CreateProject;
import mc.projectmanager.project.impl.ProjectCommand.DeleteProject;
import mc.projectmanager.project.impl.ProjectCommand.EditProject;
import mc.projectmanager.project.impl.ProjectCommand.GetProject;
import mc.projectmanager.project.impl.ProjectCommand.GetProjectReply;
import mc.projectmanager.project.impl.ProjectEvent.ProjectClosed;
import mc.projectmanager.project.impl.ProjectEvent.ProjectCreated;
import mc.projectmanager.project.impl.ProjectEvent.ProjectDeleted;
import mc.projectmanager.project.impl.ProjectEvent.ProjectEdited;

/**
 * This PersistentEntity will be identified by the user
 * that's logged in and will allow to handle the commands:
	 * -create project;
	 * -get project;
	 * -edit project;
	 * -delete project;
 * and the events:
	 * -project created (will write to database with readsideprocessor);
	 * -project edited (see above);
	 * -project deleted (see above);
 * @author Mattia
 */
public class ProjectEntity extends PersistentEntity<ProjectCommand, ProjectEvent, ProjectState>{

	@Override
	public PersistentEntity<ProjectCommand, ProjectEvent, ProjectState>.Behavior initialBehavior(Optional<ProjectState> snapshotState) {
		
		BehaviorBuilder b = newBehaviorBuilder(snapshotState.orElse(new ProjectState(Optional.empty())));

		// gestione del comando di creazione di un utente
		b.setCommandHandler(CreateProject.class, 
				(cmd, ctx) -> {
					// prende il progetto dal comando lanciato, passato dal servizio
					Project project = cmd.project;
					// crea un evento progetto creato da far persistere
					ProjectEvent event = new ProjectCreated(project);
					// persiste l'evento e risponde con done
					return ctx.thenPersist(event, evt -> ctx.reply(Done.getInstance()));
				});
		
		b.setEventHandler(ProjectCreated.class,
				//quando il progetto è stato creato si assegna a questa entità un nuovo stato
				evt -> new ProjectState(Optional.of(evt.project)));
		
		b.setCommandHandler(EditProject.class, 
				(cmd, ctx) -> {
					// prende il progetto dal comando lanciato, passato dal servizio
					Project project = cmd.project;
					// crea un evento progetto creato da far persistere
					ProjectEvent event = new ProjectEdited(project);
					// persiste l'evento e risponde con done
					return ctx.thenPersist(event, evt -> ctx.reply(Done.getInstance()));
				});
		
		b.setEventHandler(ProjectEdited.class, 
				evt -> new ProjectState(Optional.of(evt.project)));
		
		b.setCommandHandler(DeleteProject.class, 
				(cmd, ctx) -> {
					Project project = state().project.get();
					ProjectEvent event = new ProjectDeleted(project);
						
					return ctx.thenPersist(event, evt -> ctx.reply(Done.getInstance()));
				});
		
		b.setEventHandler(ProjectDeleted.class, 
				//quando il progetto è stato cancellato si assegna all'entità uno stato vuoto
				evt -> new ProjectState(Optional.empty()));
		
		b.setCommandHandler(CloseProject.class, 
				(cmd, ctx) -> {
					Project project = state().project.get();
					ProjectEvent event = new ProjectClosed(project);
						
					return ctx.thenPersist(event, evt -> ctx.reply(Done.getInstance()));
				});
		
		b.setEventHandler(ProjectClosed.class, 
				evt -> new ProjectState(Optional.empty()));
		
		// gestione di comando di sola lettura
		b.setReadOnlyCommandHandler(GetProject.class, (cmd, ctx) -> {
			ctx.reply(new GetProjectReply(state().project));
		});
		
		return b.build();
	}

}
