package mc.projectmanager.project.impl;

import java.time.Instant;
import java.util.Optional;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.lightbend.lagom.javadsl.persistence.AggregateEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.serialization.Jsonable;

import mc.projectmanager.project.api.Project;

public interface ProjectEvent extends Jsonable, AggregateEvent<ProjectEvent> {

	@Override
	default public AggregateEventTag<ProjectEvent> aggregateTag() {
		return ProjectEventTag.INSTANCE;
	}

	@SuppressWarnings("serial")
	@Immutable
	@JsonDeserialize
	public class ProjectCreated implements ProjectEvent {
		public final Project project;
		public final Instant timestamp;

		public ProjectCreated(Project project) {
			this(project, Optional.empty());
		}

		@JsonCreator
		private ProjectCreated(Project project, Optional<Instant> timestamp) {
			this.project= Preconditions.checkNotNull(project, "project");
			this.timestamp = timestamp.orElseGet(() -> Instant.now());
		}

		@Override
		public boolean equals(@Nullable Object another) {
			if (this == another)
				return true;
			return another instanceof ProjectCreated && equalTo((ProjectCreated) another);
		}

		private boolean equalTo(ProjectCreated another) {
			return project.equals(another.project) && timestamp.equals(another.timestamp);
		}

		@Override
		public int hashCode() {
			int h = 31;
			h = h * 17 + project.hashCode();
			h = h * 17 + timestamp.hashCode();
			return h;
		}

		@Override
		public String toString() {
			return MoreObjects.toStringHelper("ProjectCreated").add("project", project)
					.add("timestamp", timestamp).toString();
		}
	}
	
	@SuppressWarnings("serial")
	@Immutable
	@JsonDeserialize
	public class ProjectEdited implements ProjectEvent {
		public final Project project;
		public final Instant timestamp;

		public ProjectEdited(Project project) {
			this(project, Optional.empty());
		}

		@JsonCreator
		private ProjectEdited(Project project, Optional<Instant> timestamp) {
			this.project= Preconditions.checkNotNull(project, "project");
			this.timestamp = timestamp.orElseGet(() -> Instant.now());
		}

		@Override
		public boolean equals(@Nullable Object another) {
			if (this == another)
				return true;
			return another instanceof ProjectCreated && equalTo((ProjectCreated) another);
		}

		private boolean equalTo(ProjectCreated another) {
			return project.equals(another.project) && timestamp.equals(another.timestamp);
		}

		@Override
		public int hashCode() {
			int h = 31;
			h = h * 17 + project.hashCode();
			h = h * 17 + timestamp.hashCode();
			return h;
		}

		@Override
		public String toString() {
			return MoreObjects.toStringHelper("ProjectCreated").add("project", project)
					.add("timestamp", timestamp).toString();
		}
	}
	
	@SuppressWarnings("serial")
	@Immutable
	@JsonDeserialize
	public class ProjectDeleted implements ProjectEvent {
		public final Project project;
		public final Instant timestamp;

		public ProjectDeleted(Project project) {
			this(project, Optional.empty());
		}

		@JsonCreator
		private ProjectDeleted(Project project, Optional<Instant> timestamp) {
			this.project= Preconditions.checkNotNull(project, "project");
			this.timestamp = timestamp.orElseGet(() -> Instant.now());
		}

		@Override
		public boolean equals(@Nullable Object another) {
			if (this == another)
				return true;
			return another instanceof ProjectDeleted && equalTo((ProjectDeleted) another);
		}

		private boolean equalTo(ProjectDeleted another) {
			return project.equals(another.project) && timestamp.equals(another.timestamp);
		}

		@Override
		public int hashCode() {
			int h = 31;
			h = h * 17 + project.hashCode();
			h = h * 17 + timestamp.hashCode();
			return h;
		}

		@Override
		public String toString() {
			return MoreObjects.toStringHelper("ProjectDeleted").add("project", project)
					.add("timestamp", timestamp).toString();
		}
	}

	@SuppressWarnings("serial")
	@Immutable
	@JsonDeserialize
	public class ProjectClosed implements ProjectEvent {
		public final Project project;
		public final Instant timestamp;

		public ProjectClosed(Project project) {
			this(project, Optional.empty());
		}

		@JsonCreator
		private ProjectClosed(Project project, Optional<Instant> timestamp) {
			this.project= Preconditions.checkNotNull(project, "project");
			this.timestamp = timestamp.orElseGet(() -> Instant.now());
		}

		@Override
		public boolean equals(@Nullable Object another) {
			if (this == another)
				return true;
			return another instanceof ProjectClosed && equalTo((ProjectClosed) another);
		}

		private boolean equalTo(ProjectClosed another) {
			return project.equals(another.project) && timestamp.equals(another.timestamp);
		}

		@Override
		public int hashCode() {
			int h = 31;
			h = h * 17 + project.hashCode();
			h = h * 17 + timestamp.hashCode();
			return h;
		}

		@Override
		public String toString() {
			return MoreObjects.toStringHelper("ProjectClosed").add("project", project)
					.add("timestamp", timestamp).toString();
		}
	}

}
