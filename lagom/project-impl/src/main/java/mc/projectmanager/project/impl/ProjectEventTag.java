package mc.projectmanager.project.impl;

import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;

public class ProjectEventTag {
	  public static final AggregateEventTag<ProjectEvent> INSTANCE = 
			    AggregateEventTag.of(ProjectEvent.class);
}
