package mc.projectmanager.project.impl;

import com.google.inject.AbstractModule;
import com.lightbend.lagom.javadsl.server.ServiceGuiceSupport;

import mc.projectmanager.project.api.ProjectService;

public class ProjectModule extends AbstractModule implements ServiceGuiceSupport {
  @Override
  protected void configure() {
    bindServices(serviceBinding(ProjectService.class, ProjectServiceImpl.class));
  }
}
