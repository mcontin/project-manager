package mc.projectmanager.user.api;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.lightbend.lagom.serialization.Jsonable;

@SuppressWarnings("serial")
@Immutable
@JsonDeserialize
public final class User implements Jsonable {
	
	public final String id;

	@JsonCreator
	public User(String id) {
		this.id = Preconditions.checkNotNull(id, "id");
	}

	public static User empty() {
		return new User("");
	}

	@JsonIgnore
	public boolean isEmpty() {
		return id.length() == 0;
	}
	
	@Override
	public boolean equals(@Nullable Object another) {
		if (this == another)
			return true;
		return another instanceof User && equalTo((User) another);
	}

	private boolean equalTo(User another) {
		return id.equals(another.id);
	}

	@Override
	public int hashCode() {
		int h = 31;
		h = h * 17 + id.hashCode();
		return h;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper("User")
				.add("userId", id)
				.toString();
	}
}
