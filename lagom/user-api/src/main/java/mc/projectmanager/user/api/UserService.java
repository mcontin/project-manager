package mc.projectmanager.user.api;
import static com.lightbend.lagom.javadsl.api.Service.named;
import static com.lightbend.lagom.javadsl.api.Service.namedCall;
import static com.lightbend.lagom.javadsl.api.Service.pathCall;
import static com.lightbend.lagom.javadsl.api.Service.restCall;

import org.pcollections.PSequence;

import akka.NotUsed;
import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.api.transport.Method;


/**
 * The user service.
 */

public interface UserService extends Service {

	/**
	 * Service call for getting a user.
	 */
	ServiceCall<NotUsed, User> getUser(String userId);

	/**
	 * Service call for getting the list of all users
	 * @return
	 */
	ServiceCall<NotUsed, PSequence<User>> getFilteredUsers(String userInput);
	
	/**
	 * Service call for creating a user.
	 *
	 * The request message is the User to create.
	 */
	ServiceCall<User, NotUsed> createUser();

	/**
	 * Supposedly the service call to authorize a user
	 * @return
	 */
	ServiceCall<LoginRequest, NotUsed> login();
	
	@Override
	default Descriptor descriptor() {
		// @formatter:off
		return named("friendservice")
				.with(
						namedCall("/api/users/new", this::createUser),
						pathCall("/api/users/:userInput", this::getFilteredUsers),
						pathCall("/api/users/:userId", this::getUser),
						restCall(Method.POST, "/api/login", this::login)
						)
				.withAutoAcl(true);
		// @formatter:on
	}
}