package mc.projectmanager.auth.impl;

import static java.util.concurrent.CompletableFuture.completedFuture;

import java.util.Optional;
import java.util.concurrent.CompletionStage;
import javax.inject.Inject;

import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.api.transport.Forbidden;
import com.lightbend.lagom.javadsl.persistence.cassandra.CassandraSession;
import com.lightbend.lagom.javadsl.server.HeaderServiceCall;
import com.lightbend.lagom.javadsl.server.ServerServiceCall;

import akka.NotUsed;
import akka.japi.Function;
import mc.projectmanager.auth.api.AuthService;

public class AuthServiceImpl implements AuthService{

	interface UserStorage {
		CompletionStage<Optional<Integer>> lookupUser(String username);
	}

	private final CassandraSession db;

	@Inject
	public AuthServiceImpl(CassandraSession db) {
		this.db = db;
	}
	
	public <Request, Response> ServerServiceCall<Request, Response> authenticate(
			Function<Integer, ServerServiceCall<Request, Response>> serviceCall) {
		return HeaderServiceCall.composeAsync(requestHeader -> {

			UserStorage userStorage = new UserStorage() {
				@Override
				public CompletionStage<Optional<Integer>> lookupUser(String username) {
					// TODO Auto-generated method stub
					return completedFuture(Optional.empty());
				}
			};

			// First lookup user
			CompletionStage<Optional<Integer>> userLookup = requestHeader.principal()
					.map(principal -> userStorage.lookupUser(principal.getName()))
					.orElse(completedFuture(Optional.empty()));

			// Then, if it exists, apply it to the service call

			return userLookup.thenApply(maybeUser -> {
				if (maybeUser.isPresent()) {
					try {
						return serviceCall.apply(maybeUser.get());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				throw new Forbidden("User must be authenticated to access this service call");
			});
			
		});
	}
	
	@Override
	public ServiceCall<String, Integer> authorize(ServiceCall<Object, Object> serviceCall) {
		return null;
	}

}
