Applicazione basata su microservizi web che permette di creare utenti che potranno a loro volta gestire progetti.
I progetti possono essere creati, modificati, chiusi e cancellati.
Ogni utente può vedere solo i progetti di cui è il creatore o il manager.