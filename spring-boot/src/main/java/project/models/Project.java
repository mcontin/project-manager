package project.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

import javax.annotation.Nullable;
import java.util.Date;
import java.util.Set;

/**
 * Created by mattia on 18/07/16.
 */
public class Project {

    private String code;
    private String name;
    private String description;
    private Date creationDate;
    private Date beginDate;
    private String type;
    private String managerId;
    private Set<String> tools;
    private String creatorId;

    public static Project empty() {
        return new Project();
    }

    private Project(){
        code = null;
        name = null;
        description = null;
        creationDate = null;
        beginDate = null;
        type = null;
        managerId = null;
        tools = null;
        creatorId = null;
    }

//    @JsonCreator
//    public Project(@JsonProperty("code") String code, @JsonProperty("name") String name){
//        this.code = code;
//        this.name = name;
//    }

    @JsonCreator
    public Project(@JsonProperty("code") String code,
                   @JsonProperty("name") String name,
                   @JsonProperty("description") String description,
                   @JsonProperty("creationDate") Date creationDate,
                   @JsonProperty("beginDate") Date beginDate,
                   @JsonProperty("type") String type,
                   @JsonProperty("managerId") String managerId,
                   @JsonProperty("tools") Set<String> tools,
                   @JsonProperty("creatorId") String creatorId) {
        this.code = Preconditions.checkNotNull(code, "code");
        this.name = Preconditions.checkNotNull(name, "name");
        this.description = description;
        this.creationDate = (creationDate != null)? creationDate : new Date();
        this.beginDate = Preconditions.checkNotNull(beginDate, "begin date");
        this.type = Preconditions.checkNotNull(type, "type");
        this.managerId = Preconditions.checkNotNull(managerId, "manager ID");
        this.tools = Preconditions.checkNotNull(tools, "tools");
        this.creatorId = creatorId;
    }

    public boolean exists() {
        return (code != null && creationDate != null);
    }

    @Override
    public boolean equals(@Nullable Object another) {
        if (this == another)
            return true;
        return another instanceof Project && equalTo((Project) another);
    }

    // A project is equal to another one when they have the same code
    private boolean equalTo(Project another) {
        return code.equals(another.code);
    }

    @Override
    public int hashCode() {
        int h = 31;
        h = h * 17 + code.hashCode();
        h = h * 17 + name.hashCode();
        h = h * 17 + creationDate.hashCode();
        return h;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper("Project")
                .add("code", code)
                .add("name", name)
                .add("description", description)
                .add("creationDate", creationDate)
                .add("beginDate", beginDate)
                .add("type", type)
                .add("managerId", managerId)
                .add("tools", tools)
                .add("creatorId", creatorId)
                .toString();
    }

    public String getCreatorId() {
        return creatorId;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public String getType() {
        return type;
    }

    public String getManagerId() {
        return managerId;
    }

    public Set<String> getTools() {
        return tools;
    }
}
