package project.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by mattia on 19/07/16.
 */
@ResponseStatus(HttpStatus.CONFLICT)
public class DuplicateProjectException extends Exception{

    public DuplicateProjectException(String code) {
        super("Project with code '" + code + "' already exists.");
    }

}
