package project.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by mattia on 19/07/16.
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class ProjectNotFoundException extends Exception {

    public ProjectNotFoundException(String projectCode) {
        super("Project with code '" + projectCode + "' was not found.");
    }

}
