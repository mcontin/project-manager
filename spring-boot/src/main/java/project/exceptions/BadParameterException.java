package project.exceptions;

/**
 * Created by mattia on 20/07/16.
 */
public class BadParameterException extends Exception {

    public BadParameterException(String message) {
        super(message);
    }

}
