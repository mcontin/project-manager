package project.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import project.exceptions.BadParameterException;
import project.exceptions.DuplicateProjectException;
import project.exceptions.ProjectNotFoundException;
import project.models.Project;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * Ogni microservizio dovrebbe avere una sua istanza di Redis
 */
@RestController
public class ProjectManagerService {

    @Autowired
    private RedisTemplate<String, Object> template;

    @RequestMapping(value = "/api/project/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Project getProject(@PathVariable("id") String code) throws ProjectNotFoundException {
        final String key = setProjectKeyWith(code);

        if(projectExists(key)) {

            Map<Object, Object> oAllProjectValues = template.boundHashOps(key).entries();

            String name = oAllProjectValues.get("name").toString();
            String description = oAllProjectValues.get("description").toString();
            Long creationDate = Long.parseLong(oAllProjectValues.get("creationDate").toString());
            Long beginDate = Long.parseLong(oAllProjectValues.get("beginDate").toString());
            String type = oAllProjectValues.get("type").toString();
            String managerId = oAllProjectValues.get("managerId").toString();
            final Set<String> tools = new HashSet<>();
            String strTools = (String) oAllProjectValues.get("tools");
            tools.addAll(Arrays.asList(strTools.split(",")));
            String creatorId = oAllProjectValues.get("creatorId").toString();

            //get all values one by one zzzzzzzzzzz
//
//        final String name = (String) template.opsForHash().get(key, "name");
//        final String description = (String) template.opsForHash().get(key, "description");
//        final Long creationDate = Long.parseLong((String) template.opsForHash().get(key, "creationDate"));
//        final Long beginDate = Long.parseLong((String) template.opsForHash().get(key, "beginDate"));
//        final String type = (String) template.opsForHash().get(key, "type");
//        final String managerId = (String) template.opsForHash().get(key, "managerId");
//        final Set<String> tools = new HashSet<>();
//        String strTools = (String) template.opsForHash().get(key, "tools");
//        tools.addAll(Arrays.asList(strTools.split(",")));
//        final String creatorId = (String) template.opsForHash().get(key, "creatorId");

            return new Project(code, name, description, new Date(creationDate),
                    new Date(beginDate), type, managerId, tools, creatorId);
        }

        throw new ProjectNotFoundException(code);
    }

    @RequestMapping(value = "/api/projects/new", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void createProject(@RequestBody Project project) throws DuplicateProjectException {
        final String key = setProjectKeyWith(project.getCode());

        if(!projectExists(key)) {

            final Map<String, Object> properties = new HashMap<>();

            properties.put("code", project.getCode());
            properties.put("name", project.getName());
            properties.put("description", project.getDescription());
            properties.put("creationDate", project.getCreationDate().getTime());
            properties.put("beginDate", project.getBeginDate().getTime());
            properties.put("type", project.getType());
            properties.put("managerId", project.getManagerId());
            properties.put("tools", project.getTools());
            properties.put("creatorId", project.getCreatorId());

            template.opsForHash().putAll(key, properties);

            return;
        }

        throw new DuplicateProjectException(project.getCode());
    }

    @RequestMapping(value = "/api/project/{id}/delete", method = RequestMethod.DELETE)
    public void deleteProject(@PathVariable("id") String code) throws ProjectNotFoundException {
        final String key = setProjectKeyWith(code);

        if(!projectExists(key)) {
            throw new ProjectNotFoundException(code);
        }

        template.delete(key);
    }

    @RequestMapping(value = "/api/project/{id}/edit", method = RequestMethod.PUT)
    public void editProject(@PathVariable("id") String code,
                            @RequestBody Project project) throws ProjectNotFoundException, BadParameterException {
        final String key = setProjectKeyWith(code);

        if(projectExists(key)) {
            //must check if code, creation date and creator id have been changed, if not then save

            if(!project.getCode().equals(code)) {
                throw new BadParameterException("The code cannot be changed.");
            }

            Long creationDate = Long.parseLong((String) template.opsForHash().get(key, "creationDate"));
            if(project.getCreationDate().getTime() != creationDate) {
                throw new BadParameterException("The creation date cannot be changed.");
            }

            String creatorId = (String) template.opsForHash().get(key, "creatorId");
            if(!project.getCreatorId().equals(creatorId)) {
                throw new BadParameterException("The id of the creator cannot be changed.");
            }

            final Map<String, Object> properties = new HashMap<>();

            properties.put("code", project.getCode());
            properties.put("name", project.getName());
            properties.put("description", project.getDescription());
            properties.put("creationDate", project.getCreationDate().getTime());
            properties.put("beginDate", project.getBeginDate().getTime());
            properties.put("type", project.getType());
            properties.put("managerId", project.getManagerId());
            properties.put("tools", project.getTools());
            properties.put("creatorId", project.getCreatorId());

            template.opsForHash().putAll(key, properties);

            return;
        }

        throw new ProjectNotFoundException(code);
    }

    private boolean projectExists(String code) {
        return template.hasKey(code);
    }

    private String setProjectKeyWith(String code) {
        return String.format("project:%s", code);
    }

    @ExceptionHandler
    void handleProjectNotFoundException(ProjectNotFoundException e, HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.NOT_FOUND.value(), e.getLocalizedMessage());
    }

    @ExceptionHandler
    void handleDuplicateProjectException(DuplicateProjectException e, HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.CONFLICT.value(), e.getLocalizedMessage());
    }

    @ExceptionHandler
    void handleBadParameterException(BadParameterException e, HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value(), e.getLocalizedMessage());
    }
}